<div class="navbar navbar-custom sticky navbar-fixed-top" role="navigation" id="sticky-nav">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- LOGO -->
            <a class="navbar-brand logo" href="index.html">
                <!--CLUB -->
                <!--span class="text-custom">66</span-->
                <img src="{{ asset('frontend/images/logo.jpeg') }}"  class="img-responsive img-circle" width="100px" alt="">
            </a>

        </div>
        <!-- end navbar-header -->

        <!-- menu -->
        <div class="navbar-collapse collapse" id="navbar-menu">

            <!-- Navbar right -->
            <ul class="nav navbar-nav navbar-right">

                <li class="active">
                    <a href="#home" class="nav-link">Accueil</a>
                </li>

                <li>
                    <a href="#features" class="nav-link">A propos</a>
                </li>

                <li>
                    <a href="#pricing" class="nav-link">Devenir Membre</a>
                </li>

                <li>
                    <a href="#clients" class="nav-link">Actualités</a>
                </li>

                <li>
                    <a href="#tvclub" class="nav-link">TV Club</a>
                </li>

                <li>
                    <a href="#" class="nav-link" data-toggle="modal" data-target="#loginModal">Login</a>
                </li>

                <!--li>
                    <a href="/contrat" class="nav-link">Telecharger Contrat</a>
                </li-->
            </ul>

        </div>
        <!--/Menu -->
    </div>
    <!-- end container -->
</div>
