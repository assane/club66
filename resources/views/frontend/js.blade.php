<script src="{{ asset('frontend/js/jquery-2.1.4.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('frontend/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('frontend/js/jquery.easing.1.3.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('frontend/js/owl.carousel.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('frontend/js/jquery.sticky.js') }}" type="text/javascript"></script>
<script src="{{ asset('frontend/js/jquery.app.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
<script src="{{ asset('backend/assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('backend/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('backend/assets/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.fr.js') }}"  charset="UTF-8"></script>
<script src="{{ asset('backend/assets/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.fr-CH.js') }}"  charset="UTF-8"></script>

<script type="text/javascript">
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        autoplay: true,
        autoplayTimeout: 4000,
        responsive:{
            0:{ items:1}
        }
    })
</script>
<script>
    $('.selectpicker').selectpicker();
</script>