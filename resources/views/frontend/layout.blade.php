<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('frontend/images/favicon.ico') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('titre')</title>
    @include('frontend.css')
    @yield('css')
</head>


<body data-spy="scroll" data-target="#navbar-menu">

@include('frontend.nav')

<div>
    @yield('content')
    @include('frontend.modal_login')
</div>

@include('frontend.js')
@yield('js')
</body>
</html>