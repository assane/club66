<link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link href="{{ asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('frontend/css/owl.carousel.css') }}" rel="stylesheet">
<link href="{{ asset('frontend/css/owl.theme.default.min.css') }}" rel="stylesheet">
<link href="{{ asset('frontend/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('backend/assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet">
<link href="{{ asset('backend/assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet">
<link href="{{ asset('backend/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('frontend/js/html5shiv.js') }}"></script>
<script src="{{ asset('frontend/js/respond.min.js') }}"></script>