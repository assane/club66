@foreach(\App\Centresinteret::all() as $centres  )
    <div class="checkbox-inline">
        <input type="checkbox" class="form-check-input" id="{{ $centres->centre_id }}" name="{{ $centres->centre_id }}-scolarité" >
        <label class="form-check-label">{{ $centres->centre_value }}</label>
    </div>
@endforeach
