<div class="modal fade" id="modalR" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
     aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-body">

                <img src="{{ asset('frontend/images/logo.jpeg') }}"  class="img-responsive" width="100px" alt="">

                <div class="wrapper-page">
                    <div class= card-box">

                           <div class="msgalert col-md-12"></div>

                        <hr />

                        <form id='FormRegister' action="{{ route('register_user') }}" class="form-horizontal" method="post" >
                              @csrf

                       <div class="col-xs-6">

                            <div class="form-group">
                            <div class="col-xs-12">
                                <select id="civilite"  name="civilite" class="form-control">
                                <option value=''>Selectionner la civilité</option>
                                <option value='xy'>Mr</option>
                                <option value='xx'>Mme</option>
                                </select>
                            </div>
                            </div>

                            <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" name="nom" id="nom" type="text"  placeholder="Nom">
                            </div>
                            </div>

                            <div class="form-group">
                            <div class="col-xs-12">
                                 <input class="form-control" name="prenom" id="prenom"  type="text"  placeholder="Prénom">
                            </div>
                            </div>

                            <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" name="email" id="email"  type="text" placeholder="Email">
                            </div>
                            </div>

                            <div class="form-group">
                            <div class="col-xs-12">
                               <input class="form-control" name="telephone"  id="telephone"  type="text" placeholder="Numero Téléphone">
                            </div>
                            </div>

                           <div class="form-group">
                            <div class="col-xs-12">
                               <input class="form-control" name="telephone2"  id="telephone2"  type="text" placeholder="Numero Téléphone Secondaire">
                            </div>
                            </div>

                       </div>

                       <div class="col-xs-6">

                           <div class="form-group">
                               <div class="col-xs-12">
                                   <div class="input-group date" data-provide="datepicker">
                                       <input type="text" class="form-control" placeholder="Date De Naissance" name="datedenaissance" id="datedenaissance">
                                       <div class="input-group-addon">
                                           <span class="glyphicon glyphicon-th"></span>
                                       </div>
                                   </div>
                               </div>
                           </div>


                           <div class="form-group">
                               <div class="col-xs-12">
                                   <input class="form-control" name="adresse"  id="adresse"  type="text" placeholder="Adresse">
                               </div>
                           </div>

                            <div class="form-group">
                            <div class="col-xs-12">
                                    <select id="commune_id" name="commune_id" class="form-control" >
                                    <option value="">--- Choississez votre Commune ---</option>
                                    @foreach(\App\Commune::all() as $c)
                                    <option value="{{ $c->id }}">{{ $c->commune }}</option>
                                    @endforeach
                                    </select>
                            </div>
                            </div>

                            <div class="form-group">
                            <div class="col-xs-12">
                              {!! Form::select('quartiers',[''=>'--- Choississez votre Quartier ---'],null,['class'=>'form-control']) !!}
                            </div>
                            </div>

                            <div class="form-group">
                            <div class="col-xs-12">
                                <label class="text-muted">Centres D'intérêts Par Défauts:</label>
                                <select name="centres_interets[]" id="centres_interets" class="selectpicker col-xs-12" data-style="btn-dark" multiple data-max-options="3" title="Centres d'intérêts">
                                @foreach(\App\Centresinteret::whereIn('id',[1,2,3])->get() as $c)
                                  <option selected value="{{ $c->id }}">{{ strtolower($c->centre_value) }}</option>
                                @endforeach
                                </select>
                            </div>
                            </div>

                           <div class="form-group" style="margin-top: 30px">
                               <div class="col-xs-12">
                                   <select name="centres_interets2[]" id="centres_interets2" class="selectpicker col-xs-12" data-style="btn-dark" multiple data-max-options="1" title="Centres D'intérêts Secondaires">
                                       @foreach(\App\Centresinteret::select('*')->whereNotIn('id',[1,2,3])->get() as $c)
                                           <option  value="{{ $c->id }}">{{ strtolower($c->centre_value) }}</option>
                                       @endforeach
                                   </select>
                               </div>
                           </div>

                          </div>


                            <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="checkbox checkbox-primary">
                                            <input id="checkbox-signup" type="checkbox" name="checkbox-signup">
                                            <label for="checkbox-signup">J'accepte <a href="#">les termes et conditions</a></label>
                                        </div>
                                    </div>
                            </div>

                            <hr />

                            <div class="col-xs-6">
                                    <div class="form-group text-center">
                                        <div class="col-xs-12">
                                            <button class="btn btn-info btn-block text-uppercase waves-effect waves-light" type="submit" id="submitForm">
                                                Register
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group text-center">
                                        <div class="col-xs-12">
                                            <button class="btn btn-danger btn-block text-uppercase" data-dismiss="modal" type="button">
                                                Annuler
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>

                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <p>J'ai déjà un compte ? <a href="#" id="UserAlreadyRegisterLogin" class="text-primary m-l-5"><b> Connexion </b></a></p>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
</div>

