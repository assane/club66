<div id="loginModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Login </h4>
            </div>
            <div class="modal-body">
                <div class="wrapper-page">
                    <div class=" card-box">
                        <div class="panel-heading text-center">
                            <img src="{{ asset('frontend/images/logo.jpeg') }}"  class="text-center" width="100px" alt="">
                            <!--h3 class="text-center"> Connexion to <strong class="text-custom">Club 66</strong> </h3-->
                        </div>
                        <div class="panel-body">

                            <div class="msgalert1 text-center"></div>

                            <form method="POST"  id="FormLogin" action="{{ route('login_user') }}">

                                @csrf

                                <div class="form-group row">
                                    <label for="login" class="col-md-4 col-form-label text-md-right">UserName </label>
                                    <div class="col-md-8">
                                        <input id="login_user" type="email" class="form-control" name="login_user"  autofocus>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">Mot de passe</label>
                                    <div class="col-md-8">
                                        <input id="password_user" type="password" class="form-control" name="password_user" >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-xs-4">
                                        @if (Route::has('password.request'))
                                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                                {{ 'Mot de passe oublié !' }}
                                            </a>
                                        @endif
                                    </div>
                                    <div class="col-xs-8">
                                        <button type="submit" class="btn btn-custom btn-block">
                                            {{ __('Connexion') }}
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <p>Je n'ai pas encore de compte ? <a href="#" id="UserNotHaveAccountRegister" class="text-primary m-l-5"><b>Inscrivez vous</b></a></p>
                        </div>
                    </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div>

    </div>
</div>