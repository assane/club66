@extends('backend.layout')

@section('content')

    @role('president')
        @include('backend.president.dashboard_presi')
    @endrole

    @role('client')
      @include('backend.client.dashboard_client')
    @endrole

    @role('administrateur')
      @include('backend.admin.dashboard_admin')
    @endrole

    @role('collaborateur')
        @include('backend.collaborateur.dashboard_collaborateur')
    @endrole

    @role('superviseur')
        @include('backend.superviseur.dashboard_super')
    @endrole

    @role('hyperviseur')
        @include('backend.hyperviseur.dashboard_hyper')
    @endrole

@endsection
