@extends('backend.layout')

@section('css')
    <link href="{{ asset('backend/assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@role('collaborateur')
    @section('titre_page') Transaction effectuée par {{ auth()->user()->prenom.' '.auth()->user()->nom }} @endsection
@endrole

@role('superviseur')
    @section('titre_page') Statistique Transaction {{ auth()->user()->agence->nom_agence }} @endsection
@endrole

@role('hyperviseur')
    @section('titre_page') Statistique Transaction {{ auth()->user()->agence }} @endsection
@endrole


@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Numero Cotisation</th>
                        <th>Montant </th>
                        <th>Membre </th>
                        <th>Mois Cotisation</th>
                        <th>Agence</th>
                        <th>Date Dêpot</th>
                        <th>Centre Interêt</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($stats_all_cotisation_by_profile as $m)
                        <tr class="text-center">
                            <td>{{ $m->numero_transaction }}</td>
                            <td>{{ $m->montant }}</td>
                            <td>{{ $m->client->nom.' '.$m->client->prenom }}</td>
                            <td>{{ $m->mois_cotisation }}</td>
                            <td>{{ $m->agence->nom_agence }}</td>
                            <td>{{ $m->date_depot }}</td>
                            <td>{{ $m->centresinteret->centre_value }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

@endsection

@section('js')

    @include('backend.datatable')

@endsection