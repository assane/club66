@component('mail::message')

<center>
    <img src="https://i.ibb.co/sKDXJfc/logo.jpg" width="200px" class="img-responsive" />
</center>

<hr />

## Activation Commpte {{ $inputs['email'] }} ##

<hr>

**Bonjour {{ $inputs['prenom'].' '.$inputs['nom'] }} **

Nous vous remercions pour votre inscription

** NUMERO ABONNEMENT : {{ $inputs['num_abonne'] }} **

** Mot De Passe : {{ $inputs['mot_de_passe'] }} **

Merci de cliquer sur le lien ci-dessous afin d'activer votre compte,

@component('mail::button', ['color' => 'green','url' => $inputs['link']])
Activer Votre Compte !
@endcomponent

Merci ,<br>
{{ config('app.name') }}
@endcomponent
