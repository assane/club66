@component('mail::message')

<center>
    <img src="https://i.ibb.co/sKDXJfc/logo.jpg" width="200px" class="img-responsive" />
</center>

<hr />

Bonjour

Votre compte a été activé avec succès

@component('mail::button', ['color' => 'green','url' => 'http://dev.io:8005/' ])
Merci de vous connecter
@endcomponent


Merci ,<br>
{{ config('app.name') }}
@endcomponent