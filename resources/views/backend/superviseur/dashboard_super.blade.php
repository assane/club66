<div class="row">

    <div class="col-md-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-info pull-left">
                <i class="md md-attach-money text-info"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">
                        {{ SuperviseurController::get_Solde_Commune_Sup() }}
                    </b> FCFA</h3>
                <p class="text-muted">Total Cotisation Encaissé en Interne</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-info pull-left">
                <i class="md md-people text-info"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{ SuperviseurController::get_all_collaborateur_by_commune() }}</b></h3>
                <p class="text-muted">Total Collaborateur Interne</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-info pull-left">
                <i class="md md-attach-money text-info"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{ SuperviseurController::get_Solde_Commune_Months_Sup()  }}</b> FCFA</h3>
                <p class="text-muted">Total Paiement Mensuel</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="profile-detail card-box">
            <div>
                <img src="{{ asset('backend/assets/images/users/avatar-1.jpg') }}" alt="user-img" class="img-circle">
                <hr>
                Nom & Prenom : <h4 class="text-uppercase font-600">{{ auth()->user()->prenom.' '.auth()->user()->nom }}</h4>
                Profile : <h4 class="text-uppercase font-600">{{ auth()->user()->roles()->first()->display_name }}</h4>
            </div>
        </div>
    </div>

</div>