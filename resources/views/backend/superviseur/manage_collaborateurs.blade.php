@extends('backend.layout')

@section('css')
    <link href="{{ asset('backend/assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('titre_page') Gestion des Collaborateurs @endsection

@section('content')


    <p>
        <a type="button" class="btn btn-sm btn-purple" data-toggle="modal" data-target="#modelId">
            <i class="md md-add-circle md-2x"></i>
        </a>
    </p>

    <div class="modal fade" data-backdrop="static" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-lg">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelTitleId"> Nouveau Collaborateur</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="msgalert1 text-center"></div>

                    <div class="container-fluid">
                        <div class="row">

                            <form id='FormUser' action="{{ route('createCollaborateurSup') }}" class="form-horizontal" method="post" >

                                <div class="form-group col-md-12">
                                    <select id="civilite"  name="civilite" class="form-control">
                                        <option value=''>Selectionner la civilité</option>
                                        <option value='xy'>Mr</option>
                                        <option value='xx'>Mme</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-12">
                                    <input type="text" name="nom" placeholder="Nom" class="form-control" id="nom">
                                </div>

                                <div class="form-group col-md-12">
                                    <input type="text" name="prenom" placeholder="Prénom" class="form-control" id="prenom">
                                </div>

                                <div class="form-group col-md-12">
                                    <input type="email" name="email"  placeholder="Email" class="form-control" id="email">
                                </div>

                                <div class="form-group col-md-12">
                                    <input type="text" name="telephone"  placeholder="Téléphone" class="form-control" id="telephone">
                                </div>

                                <div class="form-group col-md-12">
                                    <input type="password" name="password"  placeholder="Mot de password" class="form-control" id="password">
                                </div>


                                <div class="form-group">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-block btn-success">Enregister</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" data-dismiss="modal" class="btn btn-block btn-danger">Annuler</button>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Prénom </th>
                        <th>Teléphone </th>
                        <th>Email</th>
                        <th>Agence</th>
                        <th>Status Compte</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($collaborateurs as $m)
                        <tr>
                            <td>{{ $m->nom }}</td>
                            <td>{{ $m->prenom }}</td>
                            <td>{{ $m->telephone }}</td>
                            <td>{{ $m->email }}</td>
                            <td>{{ $m->agence->nom_agence }}</td>
                            <td class="text-center"><?php if($m->status==1) {?><label class='label label-success'>Active</label> <?php } else { ?> <label class='label label-danger'>Inactive</label> <?php } ?></td>
                            <td>
                                <a href="#"> <i class="md md-edit"></i>   </a>
                                <a href="#"> <i class="md md-delete"></i> </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

@endsection

@section('js')

    @include('backend.datatable')

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#FormUser').submit(function(e) {
            e.preventDefault();
            var fd = new FormData(this);
            $.ajax({
                url: $(this).attr('action'),
                xhr: function() {
                    var xhr = new XMLHttpRequest();
                    return xhr;
                },
                type: 'post',
                processData: false,
                contentType: false,
                data: fd,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend :function() {
                    $(".msgalert1").html("<i class='fa fa-refresh fa-spin fa-2x fa-fw'></i><span class='sr-only'>Loading</span>'");
                },
                success: function(data) {
                    console.log(data);
                    if(data.code_status == 200 && data.status == 'success') {
                        $(".msgalert1").hide();
                        swal({
                                    title: data.status,
                                    text: data.message,
                                    type: data.status
                                },
                                function(){
                                    $('#FormUser')[0].reset();
                                    $('#modelId').modal('hide');
                                    window.location.reload();
                                }
                        );
                    }
                    if(data.status == 'error' && data.code_status == 500){
                        $(".msgalert1").hide();
                        swal({
                                    title: data.status,
                                    text: data.message,
                                    type: data.status
                                },
                                function(){
                                    $('#FormUser')[0].reset();
                                    $('#modelId').modal('hide');
                                }
                        );
                    }
                },
                error : function(jqXhr) {
                    if(jqXhr.status === 422 ) {
                        $errors = jqXhr.responseJSON
                        errorsHtml = '<div class="alert alert-danger">' + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + '<ul>';
                        $.each( $errors.errors, function( key, value ) {
                            errorsHtml += "<li><b> " + value[0] + "</b></li>";
                        });
                        errorsHtml += '</ul></div>';
                        $( '.msgalert1' ).html( errorsHtml );
                    }
                }
            });
        });
    </script>

@endsection

