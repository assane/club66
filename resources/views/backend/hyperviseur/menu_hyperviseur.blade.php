<li class="nav-link">
    <a href=" {{ route('dashboard') }}" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span> </a>
</li>

<li class="has_sub">
    <a href="javascript:void(0);" class="waves-effect"><i class="ti-user"></i> <span> Gestion utilisateurs </span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled">
        <li><a href="{{ route('manage_clients') }}">Membres</a></li>
        <li><a href="{{ route('manage_recruteurs') }}">Recruteurs</a></li>
        <li><a href="{{ route('manage_collaborateurs') }}">Collaborateurs</a></li>
        <li><a href="{{ route('manage_superviseurs') }}">Superviseurs</a></li>
        <li><a href="{{ route('manage_hyperviseurs') }}">Hyperviseurs</a></li>
        <li><a href="{{ route('manage_admins') }}">Administrateurs</a></li>
        <li><a href="{{ route('manage_role_perms') }}">Roles & Permissions</a></li>
    </ul>
</li>

<li class="has_sub">
    <a href="javascript:void(0);" class="waves-effect"><i class="md md-account-balance"></i> <span> Gestion Agences </span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled">
        <li><a href="{{ route('manage_agences') }}">Agences</a></li>
    </ul>
</li>

<li class="has_sub">
    <a href="javascript:void(0);" class="waves-effect"><i class="md md-settings"></i> <span> Paramètres </span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled">
        <li><a href="#">Système</a></li>
        <li><a href="#">Mise à jour</a></li>
    </ul>
</li>