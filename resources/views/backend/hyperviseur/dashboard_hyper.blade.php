<div class="row">
    <div class="col-md-9">

    <div class="col-md-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-info pull-left">
                <i class="md md-account-balance text-info"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{ \App\Agence::all()->count() }}</b></h3>
                <p class="text-muted">Total Agences</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-custom pull-left">
                <i class="md md-people text-custom"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{
                    HyperviseurController::get_cclients_in_allAgence()
                }}</b></h3>
                <p class="text-muted">Total Clients</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-info pull-left">
                <i class="md md-person text-info"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{
                    HyperviseurController::get_collaborateurs_in_allAgence()
                }}</b></h3>
                <p class="text-muted">Total Collaborateur</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-custom pull-left">
                <i class="md md-remove-red-eye text-custom"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{
                         HyperviseurController::get_superviseurs_in_allAgence()
                }}</b></h3>
                <p class="text-muted">Total Superviseur</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-success pull-left">
                <i class="md md-remove-red-eye text-success"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{
                       HyperviseurController::get_hyperviseurs_in_allAgence()
                }}</b></h3>
                <p class="text-muted">Total Hyperviseur</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

        <div class="col-md-3">
            <div class="widget-bg-color-icon card-box">
                <div class="bg-icon bg-icon-info pull-left">
                    <i class="md md-attach-money text-info"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark"><b class="counter">
                            {{ \App\Cotisation::all()->sum('montant')  }}
                        </b> FCFA</h3>
                    <p class="text-muted">Solde Total</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

</div>

    <div class="col-md-3">
        <div class="profile-detail card-box">
            <div>
                <img src="{{ asset('backend/assets/images/users/avatar-1.jpg') }}" alt="user-img" class="img-circle">
                <hr>
                Nom & Prenom : <h4 class="text-uppercase font-600">{{ auth()->user()->prenom.' '.auth()->user()->nom }}</h4>
                Profile : <h4 class="text-uppercase font-600">{{ auth()->user()->roles()->first()->display_name }}</h4>
            </div>
        </div>
    </div>

</div>