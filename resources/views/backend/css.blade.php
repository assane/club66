<link href="{{ asset('backend/assets/plugins/morris/morris.css') }}" rel="stylesheet" >
<link href="{{ asset('backend/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('backend/assets/css/core.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('backend/assets/css/components.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('backend/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('backend/assets/css/pages.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('backend/assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('backend/assets/plugins/custombox/css/custombox.css') }}" rel="stylesheet">
<link href="{{ asset('backend/assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet">
<link href="{{ asset('backend/assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet">
<link href="{{ asset('backend/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('backend/assets/js/modernizr.min.js') }}"></script>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>


