<div class="row">
    <div class="col-md-9">

    <div class="col-md-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-info pull-left">
                <i class="md md-account-balance text-info"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{DB::table('agences')->count()}}</b></h3>
                <p class="text-muted">Agences</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-custom pull-left">
                <i class="md md-people text-custom"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{\App\Role::find(1)->users->count()}}</b></h3>
                <p class="text-muted">Membres</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-info pull-left">
                <i class="md md-person text-info"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{\App\Role::find(2)->users->count()}}</b></h3>
                <p class="text-muted">Collaborateurs</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-custom pull-left">
                <i class="md md-remove-red-eye text-custom"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{\App\Role::find(3)->users->count()}}</b></h3>
                <p class="text-muted">Superviseurs</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-success pull-left">
                <i class="md md-remove-red-eye text-success"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{\App\Role::find(4)->users->count()}}</b></h3>
                <p class="text-muted">Hyperviseurs</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-danger pull-left">
                <i class="md md-settings text-danger"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{\App\Role::find(5)->users->count()}}</b></h3>
                <p class="text-muted">Administrateurs</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

        <div class="col-md-3">
            <div class="widget-bg-color-icon card-box">
                <div class="bg-icon bg-icon-danger pull-left">
                    <i class="md md-verified-user text-danger"></i>
                </div>
                <div class="text-right">
                    <h3 class="text-dark"><b class="counter">{{\App\Role::find(6)->users->count()}}</b></h3>
                    <p class="text-muted">Recruteurssgit</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
</div>

    <div class="col-md-3">
        <div class="profile-detail card-box">
            <div>
                <img src="{{ asset('backend/assets/images/users/avatar-1.jpg') }}" alt="user-img" class="img-circle">
                <hr>
                Nom & Prenom : <h4 class="text-uppercase font-600">{{ auth()->user()->prenom.' '.auth()->user()->nom }}</h4>
                Profile : <h4 class="text-uppercase font-600">{{ auth()->user()->roles()->first()->display_name }}</h4>
            </div>
        </div>
    </div>

</div>