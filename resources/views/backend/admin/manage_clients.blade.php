@extends('backend.layout')

@section('css')
    <link href="{{ asset('backend/assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('titre_page') Gestion des Membres @endsection

@section('content')


    <p>
        <a type="button" class="btn btn-sm btn-purple" data-toggle="modal" data-target="#modelId">
            <i class="md md-add-circle md-2x"></i>
        </a>
    </p>

    <div class="modal fade" data-backdrop="static" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-lg">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelTitleId"> Nouveau Client</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="msgalert1 text-center"></div>

                    <div class="container-fluid">
                        <div class="row">

                            <form id='FormRecru' action="{{ route('recrutements.store') }}" class="form-horizontal" method="post" >

                                <div class="form-group col-md-6">
                                    <select id="civilite"  name="civilite" class="form-control">
                                        <option value=''>Selectionner la civilité</option>
                                        <option value='xy'>Mr</option>
                                        <option value='xx'>Mme</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <input type="text" name="nom" placeholder="Nom" class="form-control" id="nom">
                                </div>

                                <div class="form-group col-md-6">
                                    <input type="text" name="prenom" placeholder="Prénom" class="form-control" id="prenom">
                                </div>

                                <div class="form-group col-md-6">
                                    <input type="email" name="email"  placeholder="Email" class="form-control" id="email">
                                </div>

                                <div class="form-group col-md-6">
                                    <input type="text" name="telephone"  placeholder="Téléphone" class="form-control" id="telephone">
                                </div>

                                <div class="form-group col-md-6">
                                    <input type="text" name="telephone2"  placeholder="Téléphone Secondaire" class="form-control" id="telephone2">
                                </div>

                                <div class="form-group col-md-6">
                                    <input type="text" name="adresse"  placeholder="Adresse" class="form-control" id="adresse">
                                </div>

                                <div class="form-group col-md-6">
                                    <div class="input-group date" data-provide="datepicker">
                                        <input type="text" class="form-control" placeholder="Date De Naissance" name="datedenaissance" id="datedenaissance">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <select id="commune_id" name="commune_id" class="form-control" >
                                        <option value="">--- Choississez votre Commune ---</option>
                                        @foreach(\App\Commune::all() as $c)
                                            <option value="{{ $c->id }}">{{ $c->commune }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    {!! Form::select('quartiers',[''=>'--- Choississez votre Quartier ---'],null,['class'=>'form-control']) !!}
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="text-muted">Centres D'intérêts Par Défauts:</label>
                                    <select name="centres_interets[]" id="centres_interets" class="selectpicker col-xs-12" data-style="btn-dark" multiple data-max-options="3" title="Centres d'intérêts">
                                        @foreach(\App\Centresinteret::whereIn('id',[1,2,3])->get() as $c)
                                            <option selected value="{{ $c->id }}">{{ strtolower($c->centre_value) }}</option>
                                        @endforeach
                                    </select>

                                    <br /><br />

                                    <select name="centres_interets2[]" id="centres_interets2" class="selectpicker col-xs-12" data-style="btn-dark" multiple data-max-options="1" title="Centres D'intérêts Secondaires">
                                        @foreach(\App\Centresinteret::select('*')->whereNotIn('id',[1,2,3])->get() as $c)
                                            <option  value="{{ $c->id }}">{{ strtolower($c->centre_value) }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-block btn-success">Enregister</button>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="button" data-dismiss="modal" class="btn btn-block btn-danger">Annuler</button>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="editMembre" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
         aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" role="document">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelTitleId"> Edition Client</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                        <div class="container-fluid">
                        <div class="row">

                            <div class="msgalert col-md-12"></div>

                            <hr />

                            <form id='FormEditMembre' action="{{ route('updateMembre') }}" class="form-horizontal" method="post" >
                                @csrf
                                <input type="hidden" name="idUser" class="form-control" id="idUser">

                                <div class="col-xs-6">

                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <select id="editCivilite"  name="editCivilite" class="form-control">
                                                <option value=''>Selectionner la civilité</option>
                                                <option value='xy'>Mr</option>
                                                <option value='xx'>Mme</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <input class="form-control" name="editNom" id="editNom" type="text"  placeholder="Nom">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <input class="form-control" name="editPrenom" id="editPrenom"  type="text"  placeholder="Prénom">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <input class="form-control" name="editEmail" id="editEmail"  type="text" placeholder="Email">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <input class="form-control" name="editTelephone"  id="editTelephone"  type="text" placeholder="Numero Téléphone">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <input class="form-control" name="editTelephone2"  id="editTelephone2"  type="text" placeholder="Numero Téléphone Secondaire">
                                        </div>
                                    </div>

                                </div>

                                <div class="col-xs-6">

                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <div class="input-group date" data-provide="datepicker">
                                                <input type="text" class="form-control" placeholder="Date De Naissance" name="editDatedenaissance" id="editDatedenaissance">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <input class="form-control" name="editAdresse"  id="editAdresse"  type="text" placeholder="Adresse">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <select id="editCommune_id" name="editCommune_id" class="form-control" >
                                                <option value="">--- Choississez votre Commune ---</option>
                                                @foreach(\App\Commune::all() as $c)
                                                    <option value="{{ $c->id }}">{{ $c->commune }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            {!! Form::select('quartiers',[''=>"--- Choississez la commune d'abord ---"],null,['class'=>'form-control','id'=>'editQuartier_id','name'=>'editQuartier_id','required']) !!}
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <select id="editStatus"  name="editStatus" class="form-control">
                                                <option value='0'>Desactiver</option>
                                                <option value='1'>Activer</option>
                                            </select>
                                         </div>
                                    </div>

                                   {{-- <div class="form-group">
                                        <div class="col-xs-12">
                                            <label class="text-muted">Centres D'intérêts Par Défauts:</label>
                                            <select name="editCentres_interets[]" id="editCentres_interets" class="selectpicker col-xs-12" data-style="btn-dark" multiple data-max-options="3" title="Centres d'intérêts">
                                                @foreach(\App\Centresinteret::whereIn('id',[1,2,3])->get() as $c)
                                                    <option selected value="{{ $c->id }}">{{ strtolower($c->centre_value) }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group" style="margin-top: 30px">
                                        <div class="col-xs-12">
                                            <select name="editCentres_interets2[]" id="editCentres_interets2" class="selectpicker col-xs-12" data-style="btn-dark" multiple data-max-options="1" title="Centres D'intérêts Secondaires">
                                                @foreach(\App\Centresinteret::select('*')->whereNotIn('id',[1,2,3])->get() as $c)
                                                    <option  value="{{ $c->id }}">{{ strtolower($c->centre_value) }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>--}}

                                </div>

                               <hr/>
                               <div class="modal-footer">

                                        <div class="form-group text-center">
                                            <div class="col-xs-12">
                                                <button class="btn btn-info btn-block text-uppercase waves-effect waves-light" type="submit" id="submitForm">
                                                    Modifier
                                                </button>
                                            </div>
                                        </div>

                                        <div class="form-group text-center">
                                            <div class="col-xs-12">
                                                <button class="btn btn-danger btn-block text-uppercase" data-dismiss="modal" type="button">
                                                    Annuler
                                                </button>
                                            </div>
                                        </div>
                                 </div>

                            </form>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Prénom </th>
                        <th>Teléphone </th>
                        <th>Email</th>
                        <th>Adresse</th>
                        <!--th>Quartier</th-->
                        <th>Status Abonnement</th>
                        <th>Status Compte</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($membres as $m)
                        <tr>
                            <td>{{ $m->nom }}</td>
                            <td>{{ $m->prenom }}</td>
                            <td>{{ $m->telephone }}</td>
                            <td>{{ $m->email }}</td>
                            <td>{{ $m->adresse }}</td>
                            <!--td>{{ $m->quartier }}</td-->
                            <td class="text-center"><?php if($m->abonnement_status==1) {?><label class='label label-success'>Inscris</label> <?php } else { ?> <label class='label label-danger'>NonInscris</label> <?php } ?></td>
                            <td class="text-center"><?php if($m->status==1) {?><label class='label label-success'>Active</label> <?php } else { ?> <label class='label label-danger'>Inactive</label> <?php } ?></td>
                            <td>
                                <a href="#" data-toggle='modal' data-target='#editMembre'
                                data-iduser="{{$m->id}}" data-nom="{{$m->nom}}" data-prenom="{{$m->prenom}}"data-civilite="{{$m->civilite}}" 
                                data-email="{{$m->email}}" data-naissance="{{$m->datedenaisssance}}" data-adresse="{{$m->adresse}}"
                                data-idagence="{{$m->agence_id}}" data-idcommune="{{$m->commune_id}}"{{-- data-idquartier="{{$m->quartier_id}}" --}}
                                data-telephone="{{$m->telephone}}" data-telephone2="{{$m->telephone2}}" data-status="{{$m->status}}"> <i class="md md-edit"></i>   </a>
                        
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

@endsection

@section('js')

    @include('backend.datatable')

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#FormRecru').submit(function(e) {
            e.preventDefault();
            var fd = new FormData(this);
            $.ajax({
                url: $(this).attr('action'),
                xhr: function() {
                    var xhr = new XMLHttpRequest();
                    return xhr;
                },
                type: 'post',
                processData: false,
                contentType: false,
                data: fd,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend :function() {
                    $(".msgalert1").html("<i class='fa fa-refresh fa-spin fa-2x fa-fw'></i><span class='sr-only'>Loading</span>'");
                },
                success: function(data) {
                    console.log(data);
                    if(data.code_status == 200 && data.status == 'success') {
                        $(".msgalert1").hide();
                        swal({
                                    title: data.status,
                                    text: data.message,
                                    type: data.status
                                },
                                function(){
                                    $('#FormRecru')[0].reset();
                                    $('#modelId').modal('hide');
                                    window.location.reload();
                                }
                        );
                    }
                    if(data.status == 'error' && data.code_status == 500){
                        $(".msgalert1").hide();
                        swal({
                                    title: data.status,
                                    text: data.message,
                                    type: data.status
                                },
                                function(){
                                    $('#FormRecru')[0].reset();
                                    $('#modelId').modal('hide');
                                }
                        );
                    }
                },
                error : function(jqXhr) {
                    if(jqXhr.status === 422 ) {
                        $errors = jqXhr.responseJSON
                        errorsHtml = '<div class="alert alert-danger">' + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + '<ul>';
                        $.each( $errors.errors, function( key, value ) {
                            errorsHtml += "<li><b> " + value[0] + "</b></li>";
                        });
                        errorsHtml += '</ul></div>';
                        $( '.msgalert1' ).html( errorsHtml );
                    }
                }
            });
        });

        
        $("select[name='commune_id']").change(function(){
            var commune_id = $(this).val();
            var token = $("input[name='csrf-token']").val();
            $.ajax({
                url: "<?php echo route('select-ajax') ?>",
                method: 'POST',
                data: {commune_id:commune_id, 'csrf-token':token},
                success: function(data) {
                    //console.log(data);
                    $("select[name='quartiers']").html('');
                    $("select[name='quartiers']").html(data.options);
                }
            });
        });

        $("select[name='editCommune_id']").change(function(){
            var commune_id = $(this).val();
            var token = $("input[name='csrf-token']").val();
            $.ajax({
                url: "<?php echo route('select-ajax') ?>",
                method: 'POST',
                data: {commune_id:commune_id, 'csrf-token':token},
                success: function(data) {
                    //console.log(data);
                    $("select[name='quartiers']").html('');
                    $("select[name='editQuartier_id']").html(data.options);
                }
            });
        });

        $('#editMembre').on('shown.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var civilite = button.data('civilite');
            var prenom = button.data('prenom');
            var nom = button.data('nom');
            var email = button.data('email');
            var telephone = button.data('telephone');
            var telephone2 = button.data('telephone2');
            var adresse = button.data('adresse');
            var naissance = button.data('naissance');        
            var status = button.data('status');
            var idCommune = button.data('idcommune');
            var idQuartier = button.data('idquartier');
            var idAgence = button.data('idagence');
            var idUser = button.data('iduser');

            var modal = $(this);
            modal.find('.modal-body #editCivilite').val(civilite);
            modal.find('.modal-body #editNom').val(nom);
            modal.find('.modal-body #editPrenom').val(prenom);
            modal.find('.modal-body #editTelephone').val(telephone);
            modal.find('.modal-body #editTelephone2').val(telephone2);
            modal.find('.modal-body #editAdresse').val(adresse);
            modal.find('.modal-body #editDatedenaissance').val(naissance);
            modal.find('.modal-body #editEmail').val(email);
            modal.find(".modal-body #editStatus").val(status);
            modal.find(".modal-body #editAgence_id").val(idAgence);
            modal.find(".modal-body #editCommune_id").val(idCommune);
            modal.find(".modal-body #editQuartier_id").val(idQuartier);

            modal.find(".modal-body #idUser").val(idUser);

        });

        $('#FormEditMembre').submit(function(e) {
            e.preventDefault();
            var fd = new FormData(this);
            var idUser = $('#idUser').val();
            fd.append('idUser', idUser);

            $.ajax({
                url: $(this).attr('action'),
                xhr: function() {
                    var xhr = new XMLHttpRequest();
                    return xhr;
                },
                type: 'post',
                processData: false,
                contentType: false,
                data: fd,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend :function() {
                    $(".msgalert1").html("<i class='fa fa-refresh fa-spin fa-2x fa-fw'></i><span class='sr-only'>Loading</span>'");
                },
                success: function(data) {
                    console.log(data);
                    if(data.code_status == 200 && data.status == 'success') {
                        $(".msgalert1").hide();
                        swal({
                                title: data.status,
                                text: data.message,
                                type: data.status
                            },
                            function(){
                                $('#FormEditMembre')[0].reset();
                                $('#editMembre').modal('hide');
                                window.location.reload();
                            }
                        );
                    }
                    if(data.status == 'error' && data.code_status == 500){
                        $(".msgalert1").hide();
                        swal({
                                title: data.status,
                                text: data.message,
                                type: data.status
                            },
                            function(){
                                $('#FormEditMembre')[0].reset();
                                $('#editMembre').modal('hide');
                            }
                        );
                    }

                },
                error : function(jqXhr) {
                    if(jqXhr.status === 422 ) {
                        $errors = jqXhr.responseJSON
                        errorsHtml = '<div class="alert alert-danger">' + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + '<ul>';
                        $.each( $errors.errors, function( key, value ) {
                            errorsHtml += "<li><b> " + value[0] + "</b></li>";
                        });
                        errorsHtml += '</ul></div>';
                        $( '.msgalert1' ).html( errorsHtml );
                    }
                }
            });
        });

    </script>

@endsection

