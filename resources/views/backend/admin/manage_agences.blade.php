@extends('backend.layout')

@section('titre_page') Gestion Agence @endsection

@section('content')

    <p>
        <a type="button" class="btn btn-sm btn-purple" data-toggle="modal" data-target="#modelId">
            <i class="md md-add-circle md-2x"></i>
        </a>
    </p>

    <div class="modal fade" data-backdrop="static" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-lg">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelTitleId"> Nouveau Client</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="msgalert1 text-center"></div>

                    <div class="container-fluid">
                        <div class="row">

                            <form id='FormCreateAgence' action="{{ route('create_agence') }}" class="form-horizontal" method="post" >

                                @csrf

                                <div class="form-group col-md-6">
                                    <input type="text" name="nom_agence" placeholder="Nom Agence" class="form-control" id="nom_agence">
                                </div>

                                <div class="form-group col-md-6">
                                    <select id="commune_id" name="commune_id" class="form-control" >
                                        <option value="">--- Choississez votre Commune ---</option>
                                        @foreach(\App\Commune::all() as $c)
                                            <option value="{{ $c->id }}">{{ $c->commune }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-block btn-success">Enregister</button>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="button" data-dismiss="modal" class="btn btn-block btn-danger">Annuler</button>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Nom Agence</th>
                        <th>Commune</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($agences as $ag)
                        <tr>
                            <td>{{ $ag->nom_agence }}</td>
                            <td>{{ $ag->commune->commune }}</td>
                        </tr>
                     @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>


@endsection


@section('js')

    @include('backend.datatable')

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#FormCreateAgence').submit(function(e) {
            e.preventDefault();
            var fd = new FormData(this);
            $.ajax({
                url: $(this).attr('action'),
                xhr: function() {
                    var xhr = new XMLHttpRequest();
                    return xhr;
                },
                type: 'post',
                processData: false,
                contentType: false,
                data: fd,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend :function() {
                    $(".msgalert1").html("<i class='fa fa-refresh fa-spin fa-2x fa-fw'></i><span class='sr-only'>Loading</span>'");
                },
                success: function(data) {
                    console.log(data);
                    if(data.code_status == 200 && data.status == 'success') {
                        $(".msgalert1").hide();
                        swal({
                                    title: data.status,
                                    text: data.message,
                                    type: data.status
                                },
                                function(){
                                    $('#FormCreateAgence')[0].reset();
                                    $('#modelId').modal('hide');
                                    window.location.reload();
                                }
                        );
                    }
                    if(data.status == 'error' && data.code_status == 500){
                        $(".msgalert1").hide();
                        swal({
                                    title: data.status,
                                    text: data.message,
                                    type: data.status
                                },
                                function(){
                                    $('#FormRecru')[0].reset();
                                    $('#modelId').modal('hide');
                                }
                        );
                    }
                },
                error : function(jqXhr) {
                    if(jqXhr.status === 422 ) {
                        $errors = jqXhr.responseJSON
                        errorsHtml = '<div class="alert alert-danger">' + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + '<ul>';
                        $.each( $errors.errors, function( key, value ) {
                            errorsHtml += "<li><b> " + value[0] + "</b></li>";
                        });
                        errorsHtml += '</ul></div>';
                        $( '.msgalert1' ).html( errorsHtml );
                    }
                }
            });
        });
    </script>

@endsection

