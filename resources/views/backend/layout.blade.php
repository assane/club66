<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('backend/assets/images/favicon_1.ico') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('titre')</title>
    @include('backend.css')
    @yield('css')
</head>


<body class="fixed-left">

<div id="wrapper">
    @include('backend.nav')
    @include('backend.side')

    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">@yield('titre_page','Tableau de bord')</h4>
                        <p class="text-muted page-title-alt"></p>
                        <p class="text-muted page-title-alt"></p>
                    </div>
                </div>

                @yield('content')

                <!-- end row -->
            </div>
        </div>
        <!-- content -->

        <footer class="footer text-right">
            © {{ date('Y') }}. All rights reserved.
        </footer>

    </div>
</div>

@include('backend.js')
@yield('js')
</body>
</html>