@extends('backend.layout')

@section('css')
    <link href="{{ asset('backend/assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/assets/plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('titre_page') Gestion des Membres @endsection

@section('content')

    @include('backend.president.addAbonnement')

    <div class="modal fade" data-backdrop="static" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-lg">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelTitleId"> Nouveau Paiement</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="msgalert1 text-center"></div>

                    <div class="container-fluid">
                        <div class="row">

                            <form id='FormCotisation' action="{{ route('post_payment') }}" class="form-horizontal" method="post" >
                                    @csrf
                                    <div class="col-md-4">
                                        <div class="text-center">
                                            <span class="title text-uppercase">Informations Personnelles</span>
                                            <hr>
                                        </div>

                                        <div>
                                            <input type="hidden"  name="client_id" id="client_id" />
                                            <input type="hidden"  name="agence_id" id="agence_id"  value="{{ auth()->user()->agence->id }}"/>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" readonly="readonly" name="num_abonne" id="num_abonne" />
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="nom" id="nom" placeholder="Nom">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="prenom" id="prenom" placeholder="Prenom">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="email" id="email" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="telephone" id="telephone" placeholder="Telephone">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="telephone2" id="telephone2" placeholder="Telephone2">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="text-center text-uppercase">
                                            <span class="title">Adresse Membre</span>
                                            <hr>
                                        </div>

                                        <div class="form-group">
                                            <input type="text" class="form-control" name="adresse" id="adresse" placeholder="Adresse">
                                        </div>

                                        <div class="form-group">
                                            <select id="commune_id" name="commune_id" class="form-control" >
                                                <option value="">--- Choississez votre Commune ---</option>
                                                @foreach(\App\Commune::all() as $c)
                                                    <option value="{{ $c->id }}">{{ $c->commune }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <select id="quartier_id" name="quartier_id" class="form-control" >
                                                <option value="">--- Choississez votre Quartier ---</option>
                                                @foreach(\App\Quartier::all() as $c)
                                                    <option value="{{ $c->id }}">{{ $c->quartier }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="text-center">
                                            <span class="title text-uppercase">Centres Interêts :</span>
                                            <hr>
                                        </div>

                                        <div class="form-group">
                                            <select id="locality-dropdown" name="locality" class="form-control">
                                                <option value="">Centres d'interêts</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">


                                        <div class="text-center">
                                            <span class="title text-uppercase">Cotisation :</span>
                                            <hr>
                                        </div>

                                        <div class="form-group text-center">
                                            <label class="radio-inline"><input type="radio" class="payment_periode_jour" name="payment_periode" checked>Jour</label>
                                            <label class="radio-inline"><input type="radio" class="payment_periode_semaine" name="payment_periode">Semaine</label>
                                            <label class="radio-inline"><input type="radio" class="payment_periode_mois" name="payment_periode">Mois</label>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" name="montant_cotisation" id="montant_cotisation" placeholder="Montant Cotisation">
                                            </div>
                                            <div class="col-md-2" style="padding: 10px">
                                                <label class="">FCFA</label>
                                            </div>
                                        </div>

                                        <!--div class="form-group">
                                            <div class="input-group date" data-provide="datepicker2" id="datepicker2">
                                                <input type="text" class="form-control" placeholder="Mois_Cotisation" name="mois_cotisation" id="mois_cotisation">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                        </div-->

                                        <div class="form-group">
                                            <button class="btn btn-success btn-block" type="submit">
                                                Valider
                                            </button>
                                            <button class="btn btn-danger btn-block" type="button">
                                                Annuler
                                            </button>
                                        </div>

                                    </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Prénom </th>
                        <th>Teléphone </th>
                        <th>Email</th>
                        <th>Adresse</th>
                        <!--th>Quartier</th-->
                        <th>Status Abonnement</th>
                        <th>Status Compte</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($membres as $m)
                        <tr <?php if($m->abonnement_status==0){ echo "class='danger'"; }?>  >
                            <td>{{ $m->nom }}</td>
                            <td>{{ $m->prenom }}</td>
                            <td>{{ $m->telephone }}</td>
                            <td>{{ $m->email }}</td>
                            <td>{{ $m->adresse }}</td>
                            <!--td>{{ $m->quartier }}</td-->
                            <td class="text-center"><?php if($m->abonnement_status==1) {?><label class='label label-success'>Inscris</label> <?php } else { ?> <label class='label label-danger'>NonInscris</label> <?php } ?></td>
                            <td class="text-center"><?php if($m->status==1) {?><label class='label label-success'>Active</label> <?php } else { ?> <label class='label label-danger'>Inactive</label> <?php } ?></td>
                            <td class="text-center">
                                @if($m->abonnement_status==1)
                                    <a href="#" class="btn btn-sm btn-purple" id="client_{{$m->id}}" onclick="load_user_for_Payment(this.id)" data-toggle="modal" data-target="#modelId">
                                         <i class="md md-add-circle"></i> Paiement
                                    </a>
                                @endif
                                @if($m->abonnement_status==0)
                                    <a href="#" class="btn btn-sm btn-danger" id="client_{{$m->id}}" onclick="load_user_for_Inscription(this.id)" data-toggle="modal" data-target="#">
                                        <i class="md md-add-circle"></i> Inscrire
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

@endsection

@section('js')

    @include('backend.datatable')

    <script>

        $(".payment_periode_jour").change(function() {
            $("#montant_cotisation").val("");
            $('#montant_cotisation').prop('readonly', false);
            $("#montant_cotisation").focus();
        });

        $(".payment_periode_semaine").change(function() {
            $("#montant_cotisation").val('225');
            $('#montant_cotisation').prop('readonly', true);
        });

        $(".payment_periode_mois").change(function() {
            $("#montant_cotisation").val('900');
            $('#montant_cotisation').prop('readonly', true);
        });

        $("#datepicker2").datepicker( {
            format: "mm-yyyy",
            viewMode: "months",
            minViewMode: "months"
        });

        $(".datepicker3").datepicker( {
            format: "mm-yyyy",
            viewMode: "months",
            minViewMode: "months"
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $("select[name='commune_id']").change(function(){
            var commune_id = $(this).val();
            var token = $("input[name='csrf-token']").val();
            $.ajax({
                url: "<?php echo route('select-ajax') ?>",
                method: 'POST',
                data: {commune_id:commune_id, 'csrf-token':token},
                success: function(data) {
                    //console.log(data);
                    $("select[name='quartier_id'").html('');
                    $("select[name='quartier_id'").html(data.options);
                }
            });
        });

        function load_user_for_Payment(id){
            var id_user = id;
            var res = id_user.split("_");
            var dataString = 'id_user='+ res[1] ;
            $('#locality-dropdown').find('option').remove().end();
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('load_member_for_payment') }}",
                data: dataString,
                cache: false,
                async:true,
                success: function(data) {
                    console.log(data);
                    $("#nom").val(data.nom);
                    $("#num_abonne").val(data.num_abonne);
                    $("#prenom").val(data.prenom);
                    $("#email").val(data.email);
                    $("#telephone").val(data.telephone);
                    $("#telephone2").val(data.telephone2);
                    $("#adresse").val(data.adresse);
                    $("#commune_id").val(data.commune_id);
                    $("#quartier_id").val(data.quartier_id);
                    $.each(data.centres, function(index, value){
                        $("#locality-dropdown").append('<option value="' + value.id + '">' + value.centre_value  + '</option>');
                    });
                    $("#client_id").val(data.id)
                }
            });
        }

        function load_user_for_Inscription(id){
            var id_user = id;
            var res = id_user.split("_");
            var dataString = 'id_user='+ res[1];
            $('#client_id').val(res[1]);
            $('#modalAbonnement').modal('show');
        }

        $('#FormCotisation').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: $(this).attr('action'),
                xhr: function() {
                    var xhr = new XMLHttpRequest();
                    return xhr;
                },
                data: {
                      'montant': $("#montant_cotisation").val(),
                      'nom': $("#nom").val(),
                      'num_abonne': $("#num_abonne").val(),
                      'prenom': $("#prenom").val(),
                      'email': $("#email").val(),
                      'telephone': $("#telephone").val(),
                      'telephone2': $("#telephone2").val(),
                      'adresse':  $("#adresse").val(),
                      'commune_id': $("#commune_id").val(),
                      'quartier_id': $("#quartier_id").val(),
                      'centresinteret_id': $("#locality-dropdown").val(),
                      'client_id': $("#client_id").val(),
                      'agence_id': $("#agence_id").val()
                },
                cache: false,
                async:true,
                success: function(data) {
                    console.log(data);
                    if(data.status == "success" && data.code_status == 200 ){
                                 swal({
                                    title: data.status,
                                    text: data.message,
                                    type: data.status
                                },
                                function(){
                                    $('#FormCotisation')[0].reset();
                                    $('#modelId').modal('hide');
                                });

                    }
                    if(data.status == "error" && data.code_status == 500 ){
                                 swal({
                                    title: data.status,
                                    text: data.message,
                                    type: data.status
                                },
                                function(){
                                    $('#FormCotisation')[0].reset();
                                    $('#modelId').modal('hide');
                                });
                    }
                },
                error : function(jqXhr) {
                    if(jqXhr.status === 422 ) {
                        $errors = jqXhr.responseJSON
                        errorsHtml = '<div class="alert alert-danger">' + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + '<ul>';
                        $.each( $errors.errors, function( key, value ) {
                            errorsHtml += "<li><b> " + value[0] + "</b></li>";
                        });
                        errorsHtml += '</ul></div>';
                        $( '.msgalert1' ).html( errorsHtml );
                    }
                }
            });

        });

        $('#frm-save-abonnement').submit(function(e) {
            e.preventDefault();
            var fd = new FormData(this);
            $.ajax({
                url: $(this).attr('action'),
                xhr: function() {
                    var xhr = new XMLHttpRequest();
                    return xhr;
                },
                type: 'post',
                processData: false,
                contentType: false,
                data: fd,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend :function() {
                    $(".msgalert1").html("<i class='fa fa-refresh fa-spin fa-2x fa-fw'></i><span class='sr-only'>Loading</span>'");
                },
                success: function(data) {
                    console.log(data);
                    if(data.code_status == 200 && data.status == 'success') {
                        $(".msgalert1").hide();
                        swal({
                                    title: data.status,
                                    text: data.message,
                                    type: data.status
                                },
                                function(){
                                    $('#frm-save-abonnement')[0].reset();
                                    $('#modalAbonnement').modal('hide');
                                    window.location.reload();
                                }
                        );
                    }
                    if(data.status == 'error' && data.code_status == 500){
                        $(".msgalert1").hide();
                        swal({
                                    title: data.status,
                                    text: data.message,
                                    type: data.status
                                },
                                function(){
                                    $('#frm-save-abonnement')[0].reset();
                                    $('#modelId').modal('hide');
                                }
                        );
                    }
                },
                error : function(jqXhr) {
                    if(jqXhr.status === 422 ) {
                        $errors = jqXhr.responseJSON
                        errorsHtml = '<div class="alert alert-danger">' + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + '<ul>';
                        $.each( $errors.errors, function( key, value ) {
                            errorsHtml += "<li><b> " + value[0] + "</b></li>";
                        });
                        errorsHtml += '</ul></div>';
                        $( '.msgalert1' ).html( errorsHtml );
                    }
                }
            });
        });




    </script>

@endsection

