<li class="nav-link">
    <a href=" {{ route('dashboard') }}" class="waves-effect"><i class="ti-home"></i> <span> Dashboard</span> </a>
</li>

<li class="nav-link">
    <a href="{{ route('get_membres') }}" class="waves-effect"><i class="ti-list"></i> <span> Effectuer paiement </span> </a>
</li>

<li class="nav-link">
    <a href="{{ route('stats_transaction_byProfile') }}" class="waves-effect"><i class="ti-eye"></i> <span>Consultations</span> </a>
</li>
