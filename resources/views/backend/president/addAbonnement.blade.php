<div class="modal fade" data-backdrop="static" id="modalAbonnement" tabindex="-1" role="dialog" aria-labelledby="abonnementTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title" id="abonnementTitle"> Nouveau Abonnement</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="container-fluid">
                    <div class="row">

                        <form id='frm-save-abonnement' action="{{ route('abonnements.store') }}" class="form-horizontal" method="post" >

                            <input id="client_id"  name="client_id" type="hidden"  />

                            <div class="form-group col-md-6">
                                <input type="text" name="montantAbonnement" value="10000" readonly="true" class="form-control" id="montantAbonnement">
                            </div>

                            <div class="form-group col-md-6">
                                <div class="input-group date" data-provide="datepicker">
                                    <input type="text" class="form-control" placeholder="Date" name="date_abonnement" id="date_abonnement">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <div class="input-group date yearPicker" >
                                    <input type="text" class="form-control" placeholder="Annee" name="annee_abonnement" id="annee_abonnement">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-block btn-success">Enregister</button>
                                </div>
                                <div class="col-md-3">
                                    <button type="button" data-dismiss="modal" class="btn btn-block btn-danger">Annuler</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>