<div class="modal fade" data-backdrop="static" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title" id="modelTitleId"> Nouveau Client</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

              <div class="msgalert1 text-center"></div>

                <div class="container-fluid">
                    <div class="row">

                        <form id='FormRecru' action="{{ route('recrutements.store') }}" class="form-horizontal" method="post" >


                            <div class="form-group col-md-6">
                                <select id="civilite"  name="civilite" class="form-control">
                                    <option value=''>Selectionner la civilité</option>
                                    <option value='xy'>Mr</option>
                                    <option value='xx'>Mme</option>
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <input type="text" name="nom" placeholder="Nom" class="form-control" id="nom">
                            </div>

                            <div class="form-group col-md-6">
                                <input type="text" name="prenom" placeholder="Prénom" class="form-control" id="prenom">
                            </div>

                            <div class="form-group col-md-6">
                                <input type="email" name="email"  placeholder="Email" class="form-control" id="email">
                            </div>

                            <div class="form-group col-md-6">
                                <input type="text" name="telephone"  placeholder="Téléphone" class="form-control" id="telephone">
                            </div>

                            <div class="form-group col-md-6">
                                <input type="text" name="telephone2"  placeholder="Téléphone Secondaire" class="form-control" id="telephone2">
                            </div>

                            <div class="form-group col-md-6">
                                <input type="text" name="adresse"  placeholder="Adresse" class="form-control" id="adresse">
                            </div>

                            <div class="form-group col-md-6">
                                <div class="input-group date" data-provide="datepicker">
                                    <input type="text" class="form-control" placeholder="Date De Naissance" name="datedenaissance" id="datedenaissance">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <select id="commune_id" name="commune_id" class="form-control" >
                                    <option value="">--- Choississez votre Commune ---</option>
                                    @foreach(\App\Commune::all() as $c)
                                        <option value="{{ $c->id }}">{{ $c->commune }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                {!! Form::select('quartiers',[''=>'--- Choississez votre Quartier ---'],null,['class'=>'form-control']) !!}
                            </div>

                            <div class="form-group col-md-6">
                                <label class="text-muted">Centres D'intérêts Par Défauts:</label>
                                <select name="centres_interets[]" id="centres_interets" class="selectpicker col-xs-12" data-style="btn-dark" multiple data-max-options="3" title="Centres d'intérêts">
                                    @foreach(\App\Centresinteret::whereIn('id',[1,2,3])->get() as $c)
                                        <option selected value="{{ $c->id }}">{{ strtolower($c->centre_value) }}</option>
                                    @endforeach
                                </select>

                                <br /><br />

                                <select name="centres_interets2[]" id="centres_interets2" class="selectpicker col-xs-12" data-style="btn-dark" multiple data-max-options="1" title="Centres D'intérêts Secondaires">
                                    @foreach(\App\Centresinteret::select('*')->whereNotIn('id',[1,2,3])->get() as $c)
                                        <option  value="{{ $c->id }}">{{ strtolower($c->centre_value) }}</option>
                                    @endforeach
                                </select>
                            </div>



                            <div class="form-group">
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-block btn-success btnSaveMemebre">Enregister</button>
                                </div>
                                <div class="col-md-3">
                                    <button type="button" data-dismiss="modal" class="btn btn-block btn-danger">Annuler</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>