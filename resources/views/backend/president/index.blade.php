@extends('backend.layout')

@section('css')
<link href="{{ asset('backend/assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('backend/assets/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('backend/assets/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('backend/assets/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('backend/assets/plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('backend/assets/plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('backend/assets/plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('titre_page') Recrutements @endsection

@section('content')

<p><a type="button" class="btn btn-sm btn-purple" data-toggle="modal" data-target="#modelId">
        <i class="md md-add-circle md-2x"></i>
</a></p>

@include('backend.president.addClient')
@include('backend.president.addAbonnement')

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Prénom </th>
                        <th>Teléphone </th>
                        <th>Email</th>
                        <th>Date Recrutement</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($recrus as $r)
                        <tr>
                            <td>{{ $r->user->nom }}</td>
                            <td>{{ $r->user->prenom }}</td>
                            <td>{{ $r->user->telephone }}</td>
                            <td>{{ $r->user->email }}</td>
                            <td>{{ \Carbon\Carbon::parse($r->user->created_at)->format('d/m/Y') }}</td>
                            <td>
                                <a href="#"> <i class="md md-edit"></i>   </a>
                                <a href="#"> <i class="md md-delete"></i> </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

@endsection

@section('js')

    @include('backend.datatable')

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#FormRecru').submit(function (e) {
            e.preventDefault();
            var fd = new FormData(this);

            $.ajax({
                url: $(this).attr('action'),
                xhr: function() {
                    var xhr = new XMLHttpRequest();
                    return xhr;
                },
                type: 'post',
                processData: false,
                contentType: false,
                data: fd,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend :function() {
                    $(".msgalert1").html("<i class='fa fa-refresh fa-spin fa-2x fa-fw'></i><span class='sr-only'>Loading</span>");
                },
                success: function(data) {
                    console.log(data);
                        if(data.code_status == 200 && data.status == 'success') {
                            $('#modelId').modal('hide');
                            swal({
                                title: "Recrutement effectué avec succès",
                                text: "Voulez-vous proceder à l'abonnement ?",
                                type: "info",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "OUI",
                                cancelButtonText: "NON",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            }, function (isConfirm) {
                                if (isConfirm) {
                                    swal.close();
                                    $('#modalAbonnement').modal('show');
                                    $('#client_id').val(data.id_client);
                                } else {
                                    swal("Recrutement terminé", "Pensez à faire l'abonnement dés que possible", "success");
                                    window.location.reload();
                                }
                            });
                        }
                        if(data.status == 'error' && data.code_status == 500){
                            $(".msgalert1").hide();
                            swal({
                                        title: data.status,
                                        text: data.message,
                                        type: data.status
                                    },
                                    function(){
                                        $('#FormRecru')[0].reset();
                                        $('#modelId').modal('hide');
                                    }
                            );
                        }
                },
                error : function(jqXhr) {
                    if(jqXhr.status === 422 ) {
                        $errors = jqXhr.responseJSON
                        errorsHtml = '<div class="alert alert-danger">'
                                + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + '<ul>';
                        $.each( $errors.errors, function( key, value ) {
                            errorsHtml += "<li><b> " + value[0] + "</b></li>";
                        });
                        errorsHtml += '</ul></div>';
                        $( '.msgalert1' ).html( errorsHtml );
                    }
                }
            });
        });


/*        $('#frm-save-abonnement').on('submit', function(e){
            e.preventDefault();
            var client_id = $('#client_id').val();
            var data=$(this).serializeArray();
            data.push({name:"client_id",value:client_id});
            var url = $(this).attr('action');
            $.post(url, data, function (data) {
                console.log(data);
                //location.reload();
            });
            $(this).trigger('reset');
        });
*/

        $('#frm-save-abonnement').submit(function(e) {
            e.preventDefault();
            var fd = new FormData(this);
            $.ajax({
                url: $(this).attr('action'),
                xhr: function() {
                    var xhr = new XMLHttpRequest();
                    return xhr;
                },
                type: 'post',
                processData: false,
                contentType: false,
                data: fd,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend :function() {
                    $(".msgalert1").html("<i class='fa fa-refresh fa-spin fa-2x fa-fw'></i><span class='sr-only'>Loading</span>'");
                },
                success: function(data) {
                    console.log(data);
                    if(data.code_status == 200 && data.status == 'success') {
                        $(".msgalert1").hide();
                        swal({
                                    title: data.status,
                                    text: data.message,
                                    type: data.status
                                },
                                function(){
                                    $('#frm-save-abonnement')[0].reset();
                                    $('#modalAbonnement').modal('hide');
                                    window.location.reload();
                                }
                        );
                    }
                    if(data.status == 'error' && data.code_status == 500){
                        $(".msgalert1").hide();
                        swal({
                                    title: data.status,
                                    text: data.message,
                                    type: data.status
                                },
                                function(){
                                    $('#frm-save-abonnement')[0].reset();
                                    $('#modelId').modal('hide');
                                }
                        );
                    }
                },
                error : function(jqXhr) {
                    if(jqXhr.status === 422 ) {
                        $errors = jqXhr.responseJSON
                        errorsHtml = '<div class="alert alert-danger">' + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + '<ul>';
                        $.each( $errors.errors, function( key, value ) {
                            errorsHtml += "<li><b> " + value[0] + "</b></li>";
                        });
                        errorsHtml += '</ul></div>';
                        $( '.msgalert1' ).html( errorsHtml );
                    }
                }
            });
        });

        $("select[name='commune_id']").change(function(){
            var commune_id = $(this).val();
            var token = $("input[name='csrf-token']").val();
            $.ajax({
                url: "<?php echo route('select-ajax') ?>",
                method: 'POST',
                data: {commune_id:commune_id, 'csrf-token':token},
                success: function(data) {
                    //console.log(data);
                    $("select[name='quartiers']").html('');
                    $("select[name='quartiers']").html(data.options);
                }
            });
        });
    </script>

@endsection

