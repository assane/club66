<div class="row">
    <div class="col-md-3">
        <div class="widget-bg-color-icon card-box">
            <div class="bg-icon bg-icon-info pull-left">
                <i class="md md-people-outline text-info"></i>
            </div>
            <div class="text-right">
                <h3 class="text-dark"><b class="counter">{{ \App\Recrutement::where('agent_id',auth()->user()->id)->count() }}</b></h3>
                <p class="text-muted">Total Recrutement</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="profile-detail card-box col-md-9">
        <div>
            <img src="{{ asset('backend/assets/images/users/avatar-1.jpg') }}" alt="user-img" class="img-circle">
            <hr>
            Profile : <h4 class="text-uppercase font-600">{{ auth()->user()->roles()->first()->display_name }}</h4>
            Email : <h5 class="text-uppercase font-600">{{ auth()->user()->email }}</h5>
            Téléphone : <h5 class="text-uppercase font-600">{{ auth()->user()->telephone }}</h5>
        </div>
    </div>


</div>

