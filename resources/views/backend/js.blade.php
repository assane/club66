<script>
    var resizefunc = [];
</script>
<script src="{{ asset('backend/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('backend/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('backend/assets/js/detect.js') }}"></script>
<script src="{{ asset('backend/assets/js/fastclick.js') }}"></script>
<script src="{{ asset('backend/assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('backend/assets/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('backend/assets/js/waves.js') }}"></script>
<script src="{{ asset('backend/assets/js/wow.min.js') }}"></script>
<script src="{{ asset('backend/assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('backend/assets/js/jquery.scrollTo.min.js') }}"></script>
<script src="{{ asset('backend/assets/plugins/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('backend/assets/plugins/waypoints/lib/jquery.waypoints.js') }}"></script>
<script src="{{ asset('backend/assets/plugins/counterup/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('backend/assets/plugins/morris/morris.min.js') }}"></script>
<script src="{{ asset('backend/assets/plugins/raphael/raphael-min.js') }}"></script>
<script src="{{ asset('backend/assets/plugins/jquery-knob/jquery.knob.js') }}"></script>
<script src="{{ asset('backend/assets/pages/jquery.dashboard.js') }}"></script>
<script src="{{ asset('backend/assets/js/jquery.core.js') }}"></script>
<script src="{{ asset('backend/assets/js/jquery.app.js') }}"></script>
<script src="{{ asset('backend/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
<script src="{{ asset('backend/assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('backend/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.counter').counterUp({
            delay: 100,
            time: 1200
        });
        $(".knob").knob();
    });
</script>
<script>
    $('.selectpicker').selectpicker();
</script>