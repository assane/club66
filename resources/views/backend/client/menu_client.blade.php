<li class="nav-link">
    <a href=" {{ route('dashboard') }}" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span> </a>
</li>

<li class="nav-link">
    <a href="{{ route('history_client') }}" class="waves-effect"><i class="ti-list"></i> <span> Mon historique </span> </a>
</li>

<li class="nav-link">
    <a href="{{ route('payment_client') }}" class="waves-effect"><i class="ti-shopping-cart"></i> <span> Mes Paiements </span> </a>
</li>

<li class="nav-link">
    <a href="#" class="waves-effect"><i class="ti-files"></i> <span> Mes Reçus </span> </a>
</li>

<li class="nav-link">
    <a href="{{ route('inbox_client') }}" class="waves-effect"><i class="fa fa-inbox"></i> <span> Mes Courriers </span> </a>
</li>

<!--li class="nav-link">
    <a href="{{ route('reminder_client') }}" class="waves-effect"><i class="fa fa-mail-reply-all"></i> <span> Mes Relances </span> </a>
</li-->

<li class="nav-link">
    <a href="{{ route('tirage_client') }}" class="waves-effect"><i class="fa fa-random"></i> <span> Tirages </span> </a>
</li>