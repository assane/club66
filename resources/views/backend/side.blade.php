<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <div id="sidebar-menu">
            <ul>
                <li class="text-muted menu-title">Navigation</li>

                @role('president')
                   @include('backend.president.menu_presi')
                @endrole

                @role('client')
                    @include('backend.client.menu_client')
                @endrole

                @role('administrateur')
                    @include('backend.admin.menu_admin')
                @endrole

                @role('collaborateur')
                    @include('backend.collaborateur.menu_collaborateur')
                @endrole

                @role('superviseur')
                    @include('backend.superviseur.menu_superviseur')
                @endrole

                @role('hyperviseur')
                    @include('backend.hyperviseur.menu_hyperviseur')
                @endrole

            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
