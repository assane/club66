@extends('frontend.layout')

@section('titre') Club 66  @endsection

@section('content')

<section class="home bg-img-1" id="home">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="home-fullscreen">
                        <div class="full-screen">
                            <div class="home-wrapper home-wrapper-alt">
                                <h2 style="font-size: 6em" class="font-light text-white">Changing Lives</h2>
                                <h4 class="text-white">une plateforme qui permette de centraliser l’ensemble des interactions que ces derniers entretiendront avec elle et vice-versa, le Club 66 veut offrir une expérience utilisateur (client ou collaborateur) exceptionnelle tout en garantissant les niveaux les plus élevés de sécurité.</h4>
                                <a href="#features"  class="btn btn-custom nav-link">Plus d'informations</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END HOME -->


    <!-- Features -->
    <section class="section" id="features">
        <div class="container">

            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3 class="title">Les bourses de formation professionnelle</h3>
                    <p class="text-muted sub-title">Afin d’aider ceux de ses membres qui sont récemment diplômes du baccalauréat ou qui ont du interrompre des études pour des questions de moyens, le Club 66 a mis en place des bourses de formation professionnelle. ces dernières couvriront l’ensemble des couts jusqu’à la licence (pour les bacheliers ) ou l’obtention du diplôme de technicien (pour les titulaires du DEF).</p>
                    <br />
                    <h3 class="title">L’accès aux centres de santé communautaire</h3>
                    <p class="text-muted sub-title">« Faciliter l’accès aux soins a tous ses membres » est le slogan de la seconde année de fonctionnement. En effet, le Club 66 prévoit de construire, avec ses écoles, un réseau de centre de soins communautaires. Ces centres, repartis sur l’ensemble du territoire national, seront mis en place d’abord dans chacune des communes de Bamako avant d’être déployés en régions.</p>
                </div>
            </div> <!-- end row -->

            <div class="row">
                <div class="col-sm-4">
                    <div class="features-box">
                        <i class="fa fa-ambulance"></i>
                        <h4>Assurance accident</h4>
                        <p class="text-muted">La seule qualité de membre du réseau vous vaudra une couverte d’assurance complète contre les accidents de la circulation. en effet, quel que soit le mode de locomotion que vous utilisez, quel que soit le type de dommage corporel subi, vous bénéficiez d’une solide protection qui vous garantit une prise en charge hospitalière. en cas d’invalidité ou de décès, un capital vous est versé ou est mis à la disposition de vos ayants droit. Grace à l’assurance-accident du club 66, restez serein !</p>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="features-box">
                        <i class="fa fa-book"></i>
                        <h4>Frais de Scolarité</h4>
                        <p class="text-muted">Nous offrons aux enfants des membres de notre réseau, la prise en charge de leurs frais de scolarité. Jusqu’à concurrence d’un enfant par membre du Club, nous offrons gratuitement les frais d’écolage dans toutes les écoles de notre réseau, à Bamako et en régions.</p>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="features-box">
                        <i class="fa fa-dollar"></i>
                        <h4> Capital d’amorçage</h4>
                        <p class="text-muted">Toutes les semaines, comme 200 autres personnes, vous pouvez être attributaire d’un capital d’un million de francs pour lancer votre business. En effet une sélection est effectuée, hebdomadairement sur la base de l’ancienneté et d’autres critères pertinents, pour déterminer les récipiendaires de la subvention a l’entrepreneuriat. Avec les fonds, le Club vous accompagne en mettant a votre disposition le meilleurs experts pour vous conseiller dans votre stratégie d’achats et de rédaction de votre projet et de déploiement/développement de votre entreprise.</p>
                    </div>
                </div>
            </div> <!-- end row -->

            <div class="row">
                <div class="col-sm-6">
                    <div class="features-box">
                        <i class="fa fa-plane"></i>
                        <h4>Pèlerinage à la Mecque</h4>
                        <p class="text-muted">Une fois par année, le Club offrira a 200 de ses membres, la possibilité de se rendre a la Mecque pour y effectuer le Pèlerinage. Afin d’améliorer l’expérience pour ses adhérents, le Club offrira, en plus de la prise en charge des frais de voyage et d’hébergement, un pécule de 250 000 au titre d’argent de poche.
                            Avec le Club 66, accomplir le 5ème pilier de l’Islam n’a jamais été aussi facile.
                        </p>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="features-box">
                        <i class="fa fa-home"></i>
                        <h4>Obtention de Terrain viabilises</h4>
                        <p class="text-muted">1000 membres du Club bénéficieront, chaque année d’un terrain viabilise dans la périphérie de Bamako. Lesdits terrains seront viabilises et porteurs d’un titre officiel.
                            Il demeure entendu que, même si les frais de mutation sont a la charge du membre attributaire, le Club 66 mettra a la disposition de ses membres son carnet d’adresse pour obtenir les meilleurs experts agréés pour les travaux de mutation, bornage et autres formalités foncières de base.</p>
                    </div>
                </div>
            </div> <!-- end row -->
        </div> <!-- end container -->
    </section>
    <!-- end Features -->



    <!-- Features Alt -->
    <section class="section p-t-0">
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <div class="feat-description m-t-20">
                        <h4>Praesent et viverra massa non varius magna eget nibh vitae velit posuere efficitur.</h4>
                        <p class="text-muted">Ubold is a fully featured premium admin template built on top of awesome Bootstrap 3.3.7, modern web technology HTML5, CSS3 and jQuery. It has many ready to use hand crafted components. The theme is fully responsive and easy to customize. The code is super easy to understand and gives power to any developer to turn this theme into real web application. </p>
                        <a href="" class="btn btn-custom">Learn More</a>
                    </div>
                </div>
                <div class="col-sm-6 col-sm-offset-1">
                    <img src="{{ asset('frontend/images/mac_1.png') }}" alt="img" class="img-responsive m-t-20">
                </div>
            </div><!-- end row -->

        </div> <!-- end container -->
    </section>
    <!-- end features alt -->


    <!-- Features Alt -->
    <section class="section">
        <div class="container">

            <div class="row">
                <div class="col-sm-6">
                    <img src="{{ asset('frontend/images/mac_2.png') }}" alt="img" class="img-responsive">
                </div>

                <div class="col-sm-5 col-sm-offset-1">
                    <div class="feat-description">
                        <h4>Praesent et viverra massa non varius magna eget nibh vitae velit posuere efficitur.</h4>
                        <p class="text-muted">Ubold is a fully featured premium admin template built on top of awesome Bootstrap 3.3.7, modern web technology HTML5, CSS3 and jQuery. It has many ready to use hand crafted components. The theme is fully responsive and easy to customize. The code is super easy to understand and gives power to any developer to turn this theme into real web application. </p>

                        <a href="" class="btn btn-custom">Learn More</a>
                    </div>
                </div>
            </div><!-- end row -->
        </div> <!-- end container -->
    </section>
    <!-- end features alt -->


    <!-- Features Alt -->
    <section class="section">
        <div class="container">

            <div class="row">
                <div class="col-sm-5">
                    <div class="feat-description">
                        <h4>Praesent et viverra massa non varius magna eget nibh vitae velit posuere efficitur.</h4>
                        <p class="text-muted">Ubold is a fully featured premium admin template built on top of awesome Bootstrap 3.3.7, modern web technology HTML5, CSS3 and jQuery. It has many ready to use hand crafted components. The theme is fully responsive and easy to customize. The code is super easy to understand and gives power to any developer to turn this theme into real web application. </p>

                        <a href="" class="btn btn-custom">Learn More</a>
                    </div>
                </div>

                <div class="col-sm-6 col-sm-offset-1">
                    <img src="{{ asset('frontend/images/mac_3.png') }}" alt="img" class="img-responsive">
                </div>

            </div><!-- end row -->

        </div> <!-- end container -->
    </section>
    <!-- end features alt -->


    <!-- Testimonials section -->
    <section class="section bg-img-1">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <div class="owl-carousel text-center">
                        <div class="item">
                            <div class="testimonial-box">
                                <h4>Excellent support for a tricky issue related to our customization of the template. Author kept us updated as he made progress on the issue and emailed us a patch when he was done.</h4>
                                <img src="{{ asset('frontend/images/user.jpg') }}" class="testi-user img-circle" alt="testimonials-user">
                                <p>- Ubold User</p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial-box">
                                <h4>Flexible, Everything is in, Suuuuuper light, even for the code is much easier to cut and make it a theme for a productive app..</h4>
                                <img src="{{ asset('frontend/images/user2.jpg') }}" class="testi-user img-circle" alt="testimonials-user">
                                <p>- Ubold User</p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial-box">
                                <h4>Not only the code, design and support are awesome, but they also update it constantly the template with new content, new plugins. I will buy surely another coderthemes template!</h4>
                                <img src="{{ asset('frontend/images/user3.jpg') }}" class="testi-user img-circle" alt="testimonials-user">
                                <p>- Ubold User</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Testimonials section -->


    <!-- PRICING -->
    <section class="section" id="pricing">
        <div class="container">

            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3 class="title">Devenir Membre</h3>
                    <p class="text-muted sub-title">The clean and well commented code allows easy customization of the theme.It's <br> designed for describing your app, agency or business.</p>
                    <!--a class="btn btn-custom btn-lg" data-toggle="modal" data-target="#registerModal" >INSCRIPTION</a-->
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-custom btn-lg" data-toggle="modal" data-target="#modalR">
                        Inscrivez vous maintenant !!
                    </button>
                    @include('frontend.modal_register')
                </div>
            </div> <!-- end row -->
        </div> <!-- end container -->
    </section>
    <!-- End Pricing -->


    <!-- Clients -->
    <section class="section p-t-0" id="clients">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3 class="title">Actualités</h3>
                    <p class="text-muted sub-title">The clean and well commented code allows easy customization of the theme.It's <br/> designed for describing your app, agency or business.</p>
                </div>
            </div>
            <!-- end row -->

            <div class="row text-center">
                <div class="col-sm-12">
                    <ul class="list-inline client-list">
                        <li><a href="" title="Microsoft"><img src="{{ asset('frontend/images/clients/microsoft.png') }}" alt="clients"></a></li>
                        <li><a href="" title="Google"><img src="{{ asset('frontend/images/clients/google.png') }}" alt="clients"></a></li>
                        <li><a href="" title="Instagram"><img src="{{ asset('frontend/images/clients/instagram.png') }}" alt="clients"></a></li>
                        <li><a href="" title="Converse"><img src="{{ asset('frontend/images/clients/converse.png') }}" alt="clients"></a></li>
                    </ul>
                </div> <!-- end Col -->
            </div><!-- end row -->

        </div>
    </section>
    <!--End  Clients -->



<!-- Clients -->
<section class="section p-t-0" id="tvclub">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h3 class="title">TV Club</h3>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/CowsopJhX3M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                <iframe width="560" height="315" src="https://www.youtube.com/embed/CowsopJhX3M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>


    <!-- FOOTER -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <a class="navbar-brand logo" href="index.html">
                        UB<span class="text-custom">o</span>ld
                    </a>
                </div>
                <div class="col-lg-4 col-lg-offset-3 col-md-7">
                    <ul class="nav navbar-nav">
                        <li><a href="#">How it works</a></li>
                        <li><a href="#">Features</a></li>
                        <li><a href="#">Pricing</a></li>
                        <li><a href="#">Clients</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-2">
                    <ul class="social-icons">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <a href="#" class="back-to-top" id="back-to-top"> <i class="fa fa-angle-up"></i> </a>

@endsection

@section('js')

    <script type="text/javascript">


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#submitForm").attr("disabled", "disabled");

        $("#UserAlreadyRegisterLogin").click(function() {
            $("#modalR").modal('hide');
            $("#loginModal").modal('show');
        });

        $("#UserNotHaveAccountRegister").click(function() {
            $("#loginModal").modal('hide');
            $("#modalR").modal('show');
        });

        $('#FormRegister').submit(function(e) {
            e.preventDefault();
            var fd = new FormData(this);
            $.ajax({
                url: $(this).attr('action'),
                xhr: function() {
                    var xhr = new XMLHttpRequest();
                    return xhr;
                },
                type: 'post',
                processData: false,
                contentType: false,
                data: fd,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend :function() {
                    $(".msgalert").html("<i class='fa fa-refresh fa-spin fa-2x fa-fw'></i><span class='sr-only'>Loading</span>'");
                },
                success: function(data) {
                    console.log(data);

                    if(data.status == 'success'){
                        $(".msgalert").hide();
                        swal({
                                title: "Félicitation Inscription Réussie!",
                                text: "Merci de consulter votre mail afin d'activer votre compte \n" +
                                "Merci de noter votre matricule d'abonnement : "+ data.num_abonne,
                                type: data.status
                                },
                                function(){
                                    $('#FormRegister')[0].reset();
                                    $('#modalR').modal('hide');
                                }
                        );
                    }
                    if(data.status == 'error'){
                        $(".msgalert").hide();
                        swal({
                                title: "Oups : Erreur Interne !",
                                text: "Merci de contacter le support en ligne :  \n Mail support@club66.com \n Tel: +233900900900",
                                type: data.status
                                },
                                function(){
                                    $('#FormRegister')[0].reset();
                                    $('#modalR').modal('hide');
                                }
                        );
                    }
                },
                error : function (jqXhr) {
                  if(jqXhr.status === 422 ) {
                    $errors = jqXhr.responseJSON
                    errorsHtml = '<div class="alert alert-danger">' + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + '<ul>';
                    $.each( $errors.errors, function( key, value ) {
                        errorsHtml += "<li><b> " + value[0] + "</b></li>";
                    });
                    errorsHtml += '</ul></div>';
                    $( '.msgalert' ).html( errorsHtml );
                  }
                }
            });
        });

        $("#checkbox-signup").change(function() {
            if(this.checked) {
                $("#submitForm").removeAttr("disabled");
            }else{
                $("#submitForm").attr("disabled", "disabled");
            }
        });


/*        $('#FormLogin').submit(function(e) {
            e.preventDefault();
            var fd = new FormData(this);
            $.ajax({
                url: $(this).attr('action'),
                xhr: function() {
                    var xhr = new XMLHttpRequest();
                    return xhr;
                },
                type: 'post',
                processData: false,
                contentType: false,
                data: fd,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend :function() {
                    $(".msgalert1").html("<i class='fa fa-refresh fa-spin fa-2x fa-fw'></i><span class='sr-only'>Loading</span>'");
                },
                success: function(data) {
                    console.log(data);
                },
                error : function(data) {
                    console.log(data)
                }
            });
        });
*/

 $('#FormLogin').submit(function(e) {
            e.preventDefault();
            var fd = new FormData(this);
            $.ajax({
                url: $(this).attr('action'),
                xhr: function() {
                    var xhr = new XMLHttpRequest();
                    return xhr;
                },
                type: 'post',
                processData: false,
                contentType: false,
                data: fd,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend :function() {
                    $(".msgalert1").html("<i class='fa fa-refresh fa-spin fa-2x fa-fw'></i><span class='sr-only'>Loading</span>'");
                },
                success: function(data) {
                    if(data.status == 'success' && data.code_status == 200) {
                        swal({
                                    title: "Success",
                                    type: data.status
                                },
                                function(){
                                    $('.msgalert1').html('');
                                    $('#FormLogin')[0].reset();
                                    $('#modalLogin').modal('hide');
                                    window.location.href = "{{ route('dashboard') }}"
                        });
                    }
                    if(data.status == 'error') {
                        if(data.code_status == 419){
                            $('.msgalert1').html('');
                            swal({ title: "Compte Inactive", type: data.status});
                        }
                        if(data.code_status == 500){
                            $('.msgalert1').html('');
                            swal({ title: "login ou mot de passe invalide", type: data.status});
                        }

                    }
                },
                error : function(data) {
                    swal({ title: "login ou mot de passe invalide", type: data.status});
                }
            });
        });


        $("select[name='commune_id']").change(function(){
            var commune_id = $(this).val();
            var token = $("input[name='csrf-token']").val();
            $.ajax({
                url: "<?php echo route('select-ajax') ?>",
                method: 'POST',
                data: {commune_id:commune_id, 'csrf-token':token},
                success: function(data) {
                    //console.log(data);
                    $("select[name='quartiers'").html('');
                    $("select[name='quartiers'").html(data.options);
                }
            });
        });

     /*   $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            startDate: '-3d',
            language: 'fr'
        });
*/

    </script>

    <script>
        (function($){
            $.fn.datepicker.dates['fr'] = {
                days: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
                daysShort: ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
                daysMin: ["d", "l", "ma", "me", "j", "v", "s"],
                months: ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"],
                monthsShort: ["janv.", "févr.", "mars", "avril", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc."],
                today: "Aujourd'hui",
                monthsTitle: "Mois",
                clear: "Effacer",
                weekStart: 1,
                format: "dd/mm/yyyy"
            };
        }(jQuery));

        $('.datepicker').datepicker({
            language: 'fr',
            autoclose: true,
            todayHighlight: true
        })
    </script>


@endsection
