<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
                 [
                  'name' => 'user-read',
                  'display_name' => 'Read User',
                  'description' => 'Consuter les informations d\'un utilisateur'
                 ],
                 [
                  'name' => 'user-create',
                  'display_name' => 'Create User',
                  'description' => 'Création  d\'un utilisateur'
                 ],
                 [
                  'name' => 'user-edit',
                  'display_name' => 'Edit User',
                  'description' => 'Modification  d\'un utilisateur'
                 ],
                [
                 'name' => 'user-delete',
                 'display_name' => 'Delete User',
                 'description' => 'Suppression  d\'un utilisateur'
                ],
                [
                 'name' => 'sendmail',
                 'display_name' => 'Send Mail',
                 'description' => 'Envoyer un mail'
                ],
                [
                    'name' => 'do-payment',
                    'display_name' => 'Do Payment',
                    'description' => 'Faire paiement'
                ],
                [
                    'name' => 'edit-payment',
                    'display_name' => 'Edit Payment',
                    'description' => 'Modifier paiement'
                ],
                [
                    'name' => 'create-agence',
                    'display_name' => 'Create Agence',
                    'description' => 'Creer Agence'
                ],
                [
                    'name' => 'edit-agence',
                    'display_name' => 'Edit Agence',
                    'description' => 'Editer Agence'
                ],
                [
                    'name' => 'delete-agence',
                    'display_name' => 'Delete Agence',
                    'description' => 'Delete Agence'
                ],
                [
                    'name' => 'get-agence',
                    'display_name' => 'get Agence',
                    'description' => 'Voir Agence'
                ],
                [
                    'name' => 'get-stat-agence',
                    'display_name' => 'get Stat Agence',
                    'description' => 'Voir Statistique Agence'
                ],
                [
                    'name' => 'create-role',
                    'display_name' => 'Add Role',
                    'description' => 'Add Role'
                ],
                [
                    'name' => 'get-role',
                    'display_name' => 'Get Role',
                    'description' => 'Voir Role'
                ],
                [
                    'name' => 'edit-role',
                    'display_name' => 'Edit Role',
                    'description' => 'Edit Role'
                ],
                [
                    'name' => 'delete-role',
                    'display_name' => 'Edit Role',
                    'description' => 'Edit Role'
                ],
                [
                    'name' => 'get-permission',
                    'display_name' => 'Get Permission',
                    'description' => 'Get Permission'
                ],
                [
                    'name' => 'create-permission',
                    'display_name' => 'Create Permission',
                    'description' => 'Create Permission'
                ],
                [
                    'name' => 'edit-permission',
                    'display_name' => 'Edit Permission',
                    'description' => 'Edit Permission'
                ],
                [
                    'name' => 'delete-permission',
                    'display_name' => 'Delete Permission',
                    'description' => 'Detele Permission'
                ],
        ];

        foreach($permissions as $p) {
            Permission::create($p);
        }

    }
}
