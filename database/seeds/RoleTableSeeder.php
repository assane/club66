<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'client',
                'display_name' => 'client',
                'description' => ''
            ],
            [
                'name' => 'collaborateur',
                'display_name' => 'Collaborateur',
                'description' => ''
            ],
            [
                'name' => 'superviseur',
                'display_name' => 'Superviseur',
                'description' => ''
            ],
            [
                'name' => 'hyperviseur',
                'display_name' => 'Hyperviseur',
                'description' => ''
            ],
            [
                'name' => 'administrateur',
                'display_name' => 'Administrateur',
                'description' => ''
            ],
        ];

        foreach($roles as $r) {
            Role::create($r);
        }
    }
}
