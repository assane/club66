-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  mar. 05 fév. 2019 à 09:10
-- Version du serveur :  10.3.11-MariaDB
-- Version de PHP :  7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `club66_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `abonnements`
--

CREATE TABLE `abonnements` (
  `id` int(11) NOT NULL,
  `numero_transaction` varchar(100) NOT NULL DEFAULT '0',
  `montant` double NOT NULL DEFAULT 0,
  `client_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `date_abonnement` datetime DEFAULT NULL,
  `annee_abonnement` year(4) NOT NULL DEFAULT 2000
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `abonnements`
--

INSERT INTO `abonnements` (`id`, `numero_transaction`, `montant`, `client_id`, `user_id`, `date_abonnement`, `annee_abonnement`) VALUES
(1, '10029282', 10000, 100, 97, '2019-02-07 00:00:00', 2019),
(2, 'YEYZTTZ2', 10000, 101, 97, '2019-01-02 00:00:00', 2019),
(3, 'ABO_190131014154_xgn', 10000, 103, 97, '2019-01-31 00:41:54', 2019),
(4, 'ABO_190131011218_y70', 10000, 104, 97, '2019-01-31 08:12:18', 2019),
(5, 'ABO_190131011552_r1n', 10000, 105, 97, '2019-01-31 08:15:52', 2019),
(6, 'ABO_190131011837_ERW', 10000, 106, 97, '2019-01-31 08:18:37', 2019),
(8, 'ABO_190131013449_6Ne', 10000, 107, 97, '2019-01-31 19:34:49', 2019),
(9, 'ABO_190202022922_Sdz', 10000, 108, 63, '2019-02-02 23:29:22', 2019),
(10, 'ABO_190203021410_tzB', 10000, 102, 63, '2019-02-03 11:14:10', 2019),
(11, 'ABO_190203024504_B8m', 10000, 104, 111, '2019-02-03 20:45:04', 2019),
(12, 'ABO_190204021746_wYP', 10000, 100, 63, '2019-02-04 00:17:46', 2019);

-- --------------------------------------------------------

--
-- Structure de la table `agences`
--

CREATE TABLE `agences` (
  `id` int(11) NOT NULL,
  `nom_agence` varchar(50) NOT NULL DEFAULT '0',
  `commune_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `agences`
--

INSERT INTO `agences` (`id`, `nom_agence`, `commune_id`) VALUES
(1, 'Agence1', 1),
(3, 'Agence2', 2),
(4, 'Agence3', 3),
(5, 'Agence4', 4),
(6, 'Agence5', 1);

-- --------------------------------------------------------

--
-- Structure de la table `centresinterets`
--

CREATE TABLE `centresinterets` (
  `id` int(11) NOT NULL,
  `centre_id` varchar(50) NOT NULL,
  `centre_value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `centresinterets`
--

INSERT INTO `centresinterets` (`id`, `centre_id`, `centre_value`) VALUES
(1, 'checkbox-emploi', 'EMPLOI'),
(2, 'checkbox-accident', 'ASSURANCE ACCIDENT'),
(3, 'checkbox-scolarite', 'BOURSE ET SCOLARITE '),
(4, 'checkbox-capital', 'CAPITALE D\'AMORAGE'),
(5, 'checkbox-pelerinage', 'PELERINAGE A LA MECQUE'),
(6, 'checkbox-attribution', 'ATTRIBUTION DE TERRAIN'),
(7, 'checkbox-bourse', 'BOURSE DE FORMATION');

-- --------------------------------------------------------

--
-- Structure de la table `centresinteret_user`
--

CREATE TABLE `centresinteret_user` (
  `user_id` int(11) DEFAULT NULL,
  `centresinteret_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `centresinteret_user`
--

INSERT INTO `centresinteret_user` (`user_id`, `centresinteret_id`) VALUES
(73, 1),
(73, 2),
(73, 3),
(73, 4),
(74, 1),
(74, 2),
(74, 3),
(74, 7),
(76, 1),
(76, 2),
(76, 3),
(76, 6),
(88, 1),
(88, 2),
(88, 3),
(88, 5),
(89, 1),
(89, 2),
(89, 3),
(89, 7),
(100, 1),
(100, 2),
(100, 3),
(100, 6),
(101, 1),
(101, 2),
(101, 3),
(101, 7),
(102, 1),
(102, 2),
(102, 3),
(102, 5),
(103, 1),
(103, 2),
(103, 3),
(103, 5),
(104, 1),
(104, 2),
(104, 3),
(104, 4),
(105, 1),
(105, 2),
(105, 3),
(105, 5),
(106, 1),
(106, 2),
(106, 3),
(106, 6),
(107, 1),
(107, 2),
(107, 3),
(107, 5),
(108, 1),
(108, 2),
(108, 3),
(108, 6);

-- --------------------------------------------------------

--
-- Structure de la table `communes`
--

CREATE TABLE `communes` (
  `id` int(11) NOT NULL,
  `commune` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `communes`
--

INSERT INTO `communes` (`id`, `commune`) VALUES
(1, 'commune1'),
(2, 'commune2'),
(3, 'commune3'),
(4, 'commune4'),
(5, 'commune5'),
(6, 'commune6'),
(7, 'commune7'),
(8, 'commune8'),
(9, 'commune9'),
(10, 'commune10');

-- --------------------------------------------------------

--
-- Structure de la table `cotisations`
--

CREATE TABLE `cotisations` (
  `id` int(11) NOT NULL,
  `numero_transaction` varchar(100) NOT NULL,
  `montant` double NOT NULL,
  `client_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mois_cotisation` varchar(50) NOT NULL,
  `agence_id` int(11) NOT NULL,
  `date_depot` datetime NOT NULL,
  `centresinteret_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cotisations`
--

INSERT INTO `cotisations` (`id`, `numero_transaction`, `montant`, `client_id`, `user_id`, `mois_cotisation`, `agence_id`, `date_depot`, `centresinteret_id`) VALUES
(1, 'TR-020219023521-COT-QEL0X', 225, 107, 63, '02/19', 1, '2019-02-02 20:35:21', 1),
(2, 'TR-020219023611-COT-FMMLD', 225, 107, 63, '02/19', 1, '2019-02-02 20:36:11', 1),
(3, 'TR-020219024153-COT-7BZGE', 225, 107, 63, '02/19', 1, '2019-02-02 20:41:53', 1),
(4, 'TR-020219023106-COT-7ZSIZ', 225, 108, 63, '02/19', 1, '2019-02-02 23:31:06', 3),
(5, 'TR-020319021435-COT-JULUR', 900, 102, 63, '02/19', 1, '2019-02-03 11:14:35', 5),
(6, 'TR-020319024848-COT-0KJQE', 900, 102, 63, '02/19', 1, '2019-02-03 11:48:48', 5),
(7, 'TR-020319024849-COT-TYRW9', 900, 102, 63, '02/19', 1, '2019-02-03 11:48:49', 5),
(8, 'TR-020319024543-COT-R0G5X', 900, 104, 111, '02/19', 6, '2019-02-03 20:45:43', 1),
(9, 'TR-020419021828-COT-ZFOPA', 900, 100, 63, '02/19', 1, '2019-02-04 00:18:28', 2);

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_01_16_122704_entrust_setup_tables', 1),
(4, '2019_01_20_160240_create_sms_verifications_table', 2);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'user-read', 'Read User', 'Consuter les informations d\'un utilisateur', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(2, 'user-create', 'Create User', 'Création  d\'un utilisateur', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(3, 'user-edit', 'Edit User', 'Modification  d\'un utilisateur', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(4, 'user-delete', 'Delete User', 'Suppression  d\'un utilisateur', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(5, 'sendmail', 'Send Mail', 'Envoyer un mail', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(6, 'do-payment', 'Do Payment', 'Faire paiement', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(7, 'edit-payment', 'Edit Payment', 'Modifier paiement', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(8, 'create-agence', 'Create Agence', 'Creer Agence', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(9, 'edit-agence', 'Edit Agence', 'Editer Agence', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(10, 'delete-agence', 'Delete Agence', 'Delete Agence', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(11, 'get-agence', 'get Agence', 'Voir Agence', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(12, 'get-stat-agence', 'get Stat Agence', 'Voir Statistique Agence', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(13, 'create-role', 'Add Role', 'Add Role', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(14, 'get-role', 'Get Role', 'Voir Role', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(15, 'edit-role', 'Edit Role', 'Edit Role', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(16, 'delete-role', 'Edit Role', 'Edit Role', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(17, 'get-permission', 'Get Permission', 'Get Permission', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(18, 'create-permission', 'Create Permission', 'Create Permission', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(19, 'edit-permission', 'Edit Permission', 'Edit Permission', '2019-01-16 18:31:42', '2019-01-16 18:31:42'),
(20, 'delete-permission', 'Delete Permission', 'Detele Permission', '2019-01-16 18:31:42', '2019-01-16 18:31:42');

-- --------------------------------------------------------

--
-- Structure de la table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `quartiers`
--

CREATE TABLE `quartiers` (
  `id` int(11) NOT NULL,
  `quartier` varchar(50) NOT NULL,
  `commune_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `quartiers`
--

INSERT INTO `quartiers` (`id`, `quartier`, `commune_id`) VALUES
(1, 'BOULKASSAMBOUGOU', 1),
(2, 'BANCONI', 1),
(3, 'FADJIGUILA', 1),
(4, 'DOUMANZANA', 1),
(5, 'DJELIBOUGOU', 1),
(6, 'KOROFINA NORD', 1),
(7, 'KOROFINA SUD', 1),
(8, 'SOTUBA', 1),
(9, 'BAGADADJI', 2),
(10, 'SIKORONI', 2),
(11, 'NGOMI', 2),
(12, 'NIARELA', 2),
(13, 'MEDINA COURA', 2),
(14, 'MISSIRA', 2),
(15, 'BOZOLA', 2),
(16, 'ZONE INDUSTRIELLE', 2),
(17, 'HIPPODROMME', 2),
(18, 'BAKARYBOUGOU', 2),
(19, 'SANS FIL', 2),
(20, 'BOUGOUBA', 2),
(21, 'QUINZANBOUGOU', 2),
(22, 'KONEBOUGGOU', 2),
(23, 'DARSALAM', 3),
(24, 'NTOMIKOROBOUGOU', 3),
(25, 'OULOLOFOBOUGOU', 3),
(26, 'CENTRE COMMERCIAL', 3),
(27, 'BAMAKO KOURA', 3),
(28, 'BOLIBANA', 3),
(29, 'DRAVELA', 3),
(30, 'ZONE INDUSTRIELLE', 3),
(31, 'BADIALAN 2', 3),
(32, 'BADIALAN 3', 3),
(33, 'NIOMINANBOUGOU', 3),
(34, 'SOGONIFING', 3),
(35, 'SAME', 3),
(36, 'SIRAKORO DOUFING', 3),
(37, 'KOULOUBA', 3),
(38, 'POINT G', 3),
(39, 'KODABOUGOU', 3),
(40, 'KOULINIKO', 3),
(41, 'LAFIABOUGOU', 4),
(42, 'HAMDALLAYE', 4),
(43, 'DOGOUDOUMA', 4),
(44, 'GRIMGOUMO', 4),
(45, 'LASSA', 4),
(46, 'TALIKO', 4),
(47, 'SEMA 1', 5),
(48, 'BADALABOUGOU', 5),
(49, 'TOROKOROBOUGOU', 5),
(50, 'QUARTIER MALI', 5),
(51, 'BACO DJICORONI', 5),
(52, 'SABALIBOUGOU', 5),
(53, 'KALABANBOUGOU', 5),
(54, 'DAOUDABOUGOU', 5),
(55, 'SOGONIKO', 6),
(56, 'MAGNANBOUGOU', 6),
(57, 'BANANKABOUGOU', 6),
(58, 'FALADIE', 6),
(59, 'SOKORODJI', 6),
(60, 'DIANEGELA', 6),
(61, 'NIAMAKORO', 6),
(62, 'YIRIMADIO', 6),
(63, 'SENOU', 6),
(64, 'SIRAKORO-MEGETAN', 6),
(65, 'NIAMANA', 6),
(66, 'TABAKORO', 6),
(67, 'DIATOULA', 6),
(68, 'MISSABOUGOU', 6),
(69, 'DJICORONI PARA', 7),
(70, 'SEBENIKORO', 7),
(71, 'SIBIRIBOUGOU', 7),
(72, 'KALABANBOUGOU', 7),
(73, 'SAMAYA', 7),
(74, 'KANADJIGUILA', 7),
(75, 'MAMARIBOUGOU', 7),
(76, 'SAMANKO-PLANTATION', 7),
(77, 'KABALABOUGOU', 7),
(78, 'KALABANKROR', 8),
(79, 'N\'GOLOBOUGOU', 8),
(80, 'KOURALE', 8),
(81, 'GOUANA', 8),
(82, 'MISSALA', 8),
(83, 'MISSALABOUGOU', 8),
(84, 'SABALIBOUGOU', 8),
(85, 'KABALA', 8),
(86, 'DIALAKORODJI', 9),
(87, 'NOTEGEDO SIRAKORO', 9),
(88, 'SAMMASSEBOUGOU', 9),
(89, 'SANGAREBOUGOU', 9),
(90, 'SEYDOUBOUGOU', 9),
(91, 'SARAMBOUGOU', 9),
(92, 'SOULEYMANEBOUGOU', 10),
(93, 'MORIBABOUGOU', 10),
(94, 'TITIBOUGOU', 10),
(95, 'DOGOBALA', 10),
(96, 'G\'NGABAGORO', 10),
(97, 'DJINCONI', 10),
(98, 'SOLOKONO', 10),
(99, 'SIKOLOU', 10),
(100, 'SALA', 10),
(101, 'MONOUNOUBA', 10);

-- --------------------------------------------------------

--
-- Structure de la table `recrutements`
--

CREATE TABLE `recrutements` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `date_recrutement` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `recrutements`
--

INSERT INTO `recrutements` (`id`, `user_id`, `agent_id`, `date_recrutement`, `created_at`, `updated_at`) VALUES
(7, 100, 97, '2019-01-30 21:36:22', '2019-01-30 21:36:22', '2019-01-30 21:36:22'),
(8, 101, 97, '2019-01-30 21:40:51', '2019-01-30 21:40:51', '2019-01-30 21:40:51'),
(9, 102, 97, '2019-01-31 00:33:58', '2019-01-31 00:33:58', '2019-01-31 00:33:58'),
(10, 103, 97, '2019-01-31 00:37:31', '2019-01-31 00:37:31', '2019-01-31 00:37:31'),
(11, 104, 97, '2019-01-31 08:11:45', '2019-01-31 08:11:45', '2019-01-31 08:11:45'),
(12, 105, 97, '2019-01-31 08:15:36', '2019-01-31 08:15:36', '2019-01-31 08:15:36'),
(13, 106, 97, '2019-01-31 08:18:08', '2019-01-31 08:18:08', '2019-01-31 08:18:08'),
(14, 107, 97, '2019-01-31 19:33:44', '2019-01-31 19:33:44', '2019-01-31 19:33:44'),
(15, 108, 97, '2019-02-02 23:08:47', '2019-02-02 23:08:47', '2019-02-02 23:08:47');

-- --------------------------------------------------------

--
-- Structure de la table `regions`
--

CREATE TABLE `regions` (
  `id` int(11) NOT NULL,
  `region` varchar(50) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `regions`
--

INSERT INTO `regions` (`id`, `region`) VALUES
(1, 'Tombouctou '),
(2, 'Taoudéni'),
(3, 'Gao'),
(4, 'Ménaka'),
(5, 'Bamako'),
(6, ' Kayes'),
(7, 'Koulikoro'),
(8, 'Sikasso'),
(9, 'Ségou'),
(10, 'Mopti'),
(11, 'Kidal');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'client', 'client', '', '2019-01-16 18:41:42', '2019-01-16 18:41:42'),
(2, 'collaborateur', 'collaborateur', NULL, NULL, NULL),
(3, 'superviseur', 'superviseur', NULL, NULL, NULL),
(4, 'hyperviseur', 'hyperviseur', NULL, NULL, NULL),
(5, 'administrateur', 'administrateur', NULL, NULL, NULL),
(6, 'president', 'President_association', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 6),
(62, 3),
(63, 2),
(64, 4),
(65, 5),
(77, 6),
(78, 6),
(79, 6),
(80, 6),
(81, 6),
(82, 6),
(83, 6),
(84, 6),
(85, 6),
(92, 2),
(93, 2),
(94, 3),
(95, 4),
(96, 4),
(97, 6),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 2),
(110, 3),
(111, 2),
(275, 6),
(276, 6),
(277, 6),
(278, 6),
(279, 6),
(280, 6),
(281, 6),
(282, 6),
(283, 6),
(284, 6),
(285, 6),
(286, 6),
(287, 6),
(288, 6),
(289, 6),
(290, 6),
(291, 6),
(292, 6),
(293, 6),
(294, 6),
(295, 6),
(296, 6),
(297, 6),
(298, 6),
(299, 6),
(300, 6),
(301, 6),
(302, 6),
(303, 6),
(304, 6),
(305, 6),
(306, 6),
(307, 6),
(308, 6),
(309, 6),
(310, 6),
(311, 6),
(312, 6),
(313, 6),
(314, 6),
(315, 6),
(316, 6),
(317, 6),
(318, 6),
(319, 6),
(320, 6),
(321, 6),
(322, 6),
(323, 6),
(324, 6),
(325, 6),
(326, 6),
(327, 6),
(328, 6),
(329, 6),
(330, 6),
(331, 6),
(332, 6),
(333, 6),
(334, 6),
(335, 6),
(336, 6),
(337, 6),
(338, 6),
(339, 6),
(340, 6);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `civilite` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_abonne` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `abonnement_status` tinyint(4) NOT NULL DEFAULT 0,
  `datedenaisssance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationalite` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `commune_id` int(11) DEFAULT NULL,
  `quartier_id` int(11) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `activation_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `agence_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `civilite`, `nom`, `prenom`, `email`, `telephone`, `telephone2`, `num_abonne`, `status`, `abonnement_status`, `datedenaisssance`, `nationalite`, `adresse`, `region_id`, `commune_id`, `quartier_id`, `email_verified_at`, `activation_code`, `password`, `remember_token`, `created_at`, `updated_at`, `agence_id`) VALUES
(1, 'xy', 'club66dev', 'club66dev', 'club66dev@gmail.com', '221772080848', NULL, 'PRESI', 1, 0, NULL, NULL, 'Dakar Ouakam', NULL, NULL, 0, NULL, 'Y2x1Yg==', '$2y$10$z8Kkdqoo0Zo6qPZ7DzdkQ.EwQ8Sj8nXWXjLkJBF2ZTDhszte4DGvK', 'o3EBl0sSOVwWpK4oOYUX8RF7ytLUR7Q40qapTtNWSLf9JjVLhCPCX0VxPY7u', '2019-01-23 23:48:54', '2019-01-23 23:51:56', NULL),
(62, 'xy', 'Alpha Sup', 'Alpha Sup', 'supclub66@gmail.com', '221772080803', NULL, 'SUP', 1, 0, NULL, NULL, 'Dakar Ouakam', NULL, 3, 0, NULL, 'a2VpdA==', '$2y$10$LvlY8gtgkWG0.50AP3R4DemSrjOgY5v3nauD6cC32YgfpYBUkPu5q', 'NaRc2CwSofOUizcXrLNQdJdhxforMzOdHAIEzVuN5hZ6QJVtZ9sYzYEERTHs', '2019-01-24 12:58:49', '2019-01-24 13:00:28', NULL),
(63, 'xy', 'Alpha Colab', 'Alpha Colab', 'colaclub66@gmail.com', '221772080804', NULL, 'COLAB', 1, 0, NULL, NULL, 'Dakar Ouakam', NULL, NULL, 0, NULL, 'a2VpdA==', '$2y$10$LvlY8gtgkWG0.50AP3R4DemSrjOgY5v3nauD6cC32YgfpYBUkPu5q', '95nNlKbtnMoXVUNUEoXpbAVfc2TCWj5MQH2lGRpOMKKIn7c8hWInekIx4hTO', '2019-01-24 12:58:49', '2019-01-24 13:00:28', 1),
(64, 'xy', 'Alpha Hyper', 'Alpha Hyper', 'hypclub66@gmail.com', '221772080805', NULL, 'HYPER', 1, 0, NULL, NULL, 'Dakar Ouakam', NULL, NULL, 0, NULL, 'a2VpdA==', '$2y$10$LvlY8gtgkWG0.50AP3R4DemSrjOgY5v3nauD6cC32YgfpYBUkPu5q', 'dKVwLBlSeQnnbU1KSbZnV2iNbK7y9ZASs0XsAj5K2fZwqaU4uWmf22DJEEDD', '2019-01-24 12:58:49', '2019-01-24 13:00:28', NULL),
(65, 'xy', 'Alpha Admin', 'Alpha Admin', 'adminclub66@gmail.com', '221772080806', NULL, 'ADMIN', 1, 0, NULL, NULL, 'Dakar Ouakam', NULL, NULL, 0, NULL, 'a2VpdA==', '$2y$10$LvlY8gtgkWG0.50AP3R4DemSrjOgY5v3nauD6cC32YgfpYBUkPu5q', 'IaNWTNKHcm7JKjwE3TDN8UBUOEE2rmB7qMLXlqpO2PGtOQRU8plt4vnVcKqR', '2019-01-24 12:58:49', '2019-01-24 13:00:28', NULL),
(77, 'xx', 'Recruteur2', 'Recruteur2', 'Recruteur2@club66-mali.com', '22383633002', NULL, 'PRESI_2', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$qzcjsFxQ3D0FPNahRdf7Z.bdNS8n3kQ8v6oseZTcWaabE.EW.56YG', 'FcsmZxfl4CPXffWKErSlmIHvwmgvhiMXlmRAn4C781yRS8jDcSwJubqs1ktO', '2019-01-29 11:14:56', '2019-01-29 11:14:56', NULL),
(78, 'xx', 'Recruteur3', 'Recruteur3', 'Recruteur3@club66-mali.com', '22383633003', NULL, 'PRESI_3', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$JETQ3wTVC.M5mvGymeiubO31WvVqlYBnkJRTbTaWZU9TNtoi4XW0K', 'MnxAXL361Xhz8UyPd5vxuFBIROMo4Ne2AO2mAp5TQ3xIZZ1AKtNbadkBtKXw', '2019-01-29 11:14:57', '2019-01-29 11:14:57', NULL),
(79, 'xx', 'Recruteur4', 'Recruteur4', 'Recruteur4@club66-mali.com', '22383633004', NULL, 'PRESI_4', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$3LXZZByj/4ci.nP0sa.MWOqy1Apzmfx17Mg3ZzqV090oH48fERAxq', NULL, '2019-01-29 11:14:57', '2019-01-29 11:14:57', NULL),
(80, 'xx', 'Recruteur5', 'Recruteur5', 'Recruteur5@club66-mali.com', '22383633005', NULL, 'PRESI_5', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$T69FFJ8zlzOFMlcxaCvAveCqXISYg7yUKAJ.QrXxhaqEZlhQy2CIO', NULL, '2019-01-29 11:14:57', '2019-01-29 11:14:57', NULL),
(81, 'xx', 'Recruteur6', 'Recruteur6', 'Recruteur6@club66-mali.com', '22383633006', NULL, 'PRESI_6', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$Q7TLIswdlXnMEZmICLXvduDUj1GfpgUwcoqNoT.YxoVEv/KmGrwB.', NULL, '2019-01-29 11:14:57', '2019-01-29 11:14:57', NULL),
(82, 'xx', 'Recruteur7', 'Recruteur7', 'Recruteur7@club66-mali.com', '22383633007', NULL, 'PRESI_7', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$9kDN.hyTiima8e3emlcos.iwWpT4yhQtLXWs2T9yoX41vGWT71UU6', NULL, '2019-01-29 11:14:57', '2019-01-29 11:14:57', NULL),
(83, 'xx', 'Recruteur8', 'Recruteur8', 'Recruteur8@club66-mali.com', '22383633008', NULL, 'PRESI_8', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$0N5LzhrKczOeit81.CSCEueMdoqxS3ga0coiOygucEtIehEHnvvpy', NULL, '2019-01-29 11:14:57', '2019-01-29 11:14:57', NULL),
(84, 'xx', 'Recruteur9', 'Recruteur9', 'Recruteur9@club66-mali.com', '22383633009', NULL, 'PRESI_9', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$bye4EXyVkpUOqVWHQqgKFufHf9H8L/KjqnZF3RRqO6D9iPoO2FzWC', NULL, '2019-01-29 11:14:57', '2019-01-29 11:14:57', NULL),
(85, 'xx', 'Recruteur10', 'Recruteur10', 'Recruteur10@club66-mali.com', '223836330010', NULL, 'PRESI_10', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$DTsSDa6G/ycyBUJkgTSuIOcabk2aUdlMIpigJKL9GmV/.vipuAUeS', '3KNyAUEIA8JRjPMuhQvsl0sNToepcd90rozVreiJ1FGtOle1Fe8nmBHnaEU1', '2019-01-29 11:14:57', '2019-01-29 11:14:57', NULL),
(92, 'xy', 'Collaborateur1', 'Collaborateur1', 'Collaborateur1@club66-mali.com', '221772080835', NULL, 'COLAB_190129011940', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '$2y$10$rthhXoNP1zKBu1Z5MKTHW.sAzkPkwkKWYSPW64v0qHUkZ3uJL0TuO', 'YCGAgektOlMlhfzp9KDPkFsB9rdKlRfjnJLGPdX4OwfpDB824iBxatjfDjX2', '2019-01-29 21:19:40', '2019-01-29 21:19:40', 1),
(93, 'xy', 'Collaborateur2', 'Collaborateur2', 'Collaborateur2@club66-mali.com', '221772080836', NULL, 'COLAB_190129012037', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$ggvgxiKLQk3WFD.UVf5gV.EJWlwXYA6vFltmi4HhLJmMXsmIAdSKm', NULL, '2019-01-29 21:20:37', '2019-01-29 21:20:37', 3),
(94, 'xy', 'Superviseur1', 'Superviseur1', 'Superviseur1@club66-mali.com', '221772080703', NULL, 'SUP_190129010649', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '$2y$10$wjNQfQv6zVePR70xmpWimec58A1lxHSrT0cwoyZoVUdj2uxl5Gpeq', 'MS31cPwDS4IYXYriBNxNN2tiTnf7pG40wVEDX0hZNvSypzY3dd25nSdwfqXM', '2019-01-29 23:06:49', '2019-01-29 23:06:49', 1),
(95, 'xy', 'Hyperviseur1', 'Hyperviseur1', 'Hyperviseur1@club66-mali.com', '221772080601', NULL, 'HYPER_190129013544', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$As9m8QhTxGHpEHWrYbi.0emGySux/SV2soEiGPtgqyobRBgLiQTNa', 'GnzhxHubKbPaSJYUeKFSkmvjpVFcL7o7znqXsqovdqOZ8NlnFzrzGe7Il5sH', '2019-01-29 23:35:44', '2019-01-29 23:35:44', NULL),
(96, 'xy', 'Hyperviseur2', 'Hyperviseur2', 'Hyperviseur2@club66-mali.com', '221772080671', NULL, 'HYPER_190129013703', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$DoAAorM3SvR2YoUiMIIC.OMFonriGlRhRGQ9qXEvuyH/QYdM0E3dG', NULL, '2019-01-29 23:37:03', '2019-01-29 23:37:03', NULL),
(97, 'xy', 'Recruteur1', 'Recruteur1', 'Recruteur1@club66-mali.com', '221772080821', NULL, 'PRESI_190130012029', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$xZD/iVszuTaNBSqvI0aRU.Ui0zmoji8bZGKxAPf.Zr3/8Cv7nOwYu', 'pggJjYXXOXQoiZX1w0M4ZuTzAQU0im7t0ftBtbtToWEwyw7ND4oZGT8Si37Q', '2019-01-30 00:20:30', '2019-01-30 00:20:30', NULL),
(100, 'xy', 'toto', 'toto', 'toto@tot.com', '221772080320', NULL, 'TOT190130013622OTO', 1, 1, '02/01/2019', NULL, NULL, NULL, 7, 75, NULL, NULL, '$2y$10$uAxZKCRLnIKtKJwKq2luM.aaUtlT.5KZmxG6ZO.9OA0QyRfRqS0DO', NULL, '2019-01-30 21:36:22', '2019-02-04 00:17:46', NULL),
(101, 'xy', 'titi', 'titi', 'titi@titi.fr', '221775333300', NULL, 'TIT190130014051ITI', 1, 0, '01/18/2019', NULL, NULL, NULL, 6, 63, NULL, NULL, '$2y$10$7XflCyWM6kB9PNQdAvNkNuG/aiDhMR5jf8duU5ysVkcaiIr1Ft/fy', NULL, '2019-01-30 21:40:51', '2019-01-30 21:40:51', NULL),
(102, 'xy', 'bebe', 'bebe', 'bebe@bebe.fr', '221772080412', NULL, 'BEB190131013357EBE', 1, 1, '01/18/2019', NULL, 'toto adresse', NULL, 7, 69, NULL, NULL, '$2y$10$xQkkt6jxyf98UOBS.IJWRu5hARblbXwzMDsR.nIO2n.l9ru3Mi1IG', NULL, '2019-01-31 00:33:58', '2019-02-03 11:14:10', NULL),
(103, 'xy', 'bibi', 'bibi', 'bibi@bibi.fr', '221775332257', NULL, 'BIB190131013731IBI', 1, 0, '01/18/2019', NULL, 'Dakar Ouakam', NULL, 2, 13, NULL, NULL, '$2y$10$72k9ViJCplQN/z7WKtxmAudddWaxG/tH8cMjee4z/tKi1Yvi1zd1e', NULL, '2019-01-31 00:37:31', '2019-01-31 00:37:31', NULL),
(104, 'xy', 'Com1', 'Com1', 'comp@comp.com', '221774002100', NULL, 'COM190131011145OM1', 1, 1, '01/19/2019', NULL, 'Dakar Ouakam', NULL, 4, 43, NULL, NULL, '$2y$10$wdC.3nyLUCLAfv1dCapA0e4g7qxKxWUjZ5ZbEbhz536bHxuAA3hfq', NULL, '2019-01-31 08:11:45', '2019-02-03 20:45:04', NULL),
(105, 'xy', 'Com2', 'Com2', 'Com2@Com2.fr', '221772080699', NULL, 'COM190131011536OM2', 1, 0, '01/23/2019', NULL, 'Dakar Ouakam', NULL, 5, 52, NULL, NULL, '$2y$10$hwDn8gjW1v.NR5JqMnM6EOpNbhRUCOp8mJ08Nj.jwZEQG45y0HySS', NULL, '2019-01-31 08:15:36', '2019-01-31 08:15:36', NULL),
(106, 'xy', 'Com3', 'Com3', 'Com3@Com3.fr', '221772080201', NULL, 'COM190131011808OM3', 1, 0, '02/04/2014', NULL, 'dakar', NULL, 2, 15, NULL, NULL, '$2y$10$Emlm.wV4OZteRP1HpZQcluTA3RNddEPBQZoVMgid60uqwIujYw4Y6', NULL, '2019-01-31 08:18:08', '2019-01-31 08:18:08', NULL),
(107, 'xy', 'bombo', 'bombo', 'bombo@yahoo.fr', '221772080100', NULL, 'BOM190131013344MBO', 1, 1, '01/22/2019', NULL, 'Dakar Ouakam', NULL, 3, 36, NULL, NULL, '$2y$10$EItcuueBH7W0eRIafagZ1O2BQuT6./4yeYnxhfhtsXt3Qao4pu0/a', NULL, '2019-01-31 19:33:44', '2019-01-31 19:34:49', NULL),
(108, 'xy', 'pata', 'pata', 'pata@pata.fr', '221772080222', NULL, 'PAT190202020847ATA', 1, 1, '02/21/2019', NULL, NULL, NULL, 4, 43, NULL, NULL, '$2y$10$YjwdzKhoMyQ52fRY5aazHOkov.EKvqM4ZvPMMokHpuoEdJ8Ztbizq', NULL, '2019-02-02 23:08:47', '2019-02-02 23:29:22', NULL),
(109, 'xy', 'Collaborateur11', 'Collaborateur11', 'Collaborateur11@club66-mali.com', '221772028871', NULL, 'COLAB_190203021514', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$tI8wH8vcgDcu/hq6cCgIvuJZjUvWu6ThTOqNoUM.OemdD4JO7lnvy', NULL, '2019-02-03 02:15:14', '2019-02-03 02:15:14', 1),
(110, 'xy', 'Superviseur11', 'Superviseur11', 'Superviseur11@club66-mali.com', '221772060911', NULL, 'SUP_190203025358', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '$2y$10$VOwphCZOzUXOvCiEqwf.CO/EeGAW1xSM1ZPmoUbbb.T1O5Y94YDLG', NULL, '2019-02-03 19:53:58', '2019-02-03 19:53:58', NULL),
(111, 'xy', 'Collaborateur5', 'Collaborateur5', 'Collaborateur5@club66-mali.com', '221772010011', NULL, 'COLAB_190203024358', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$RUQhJDCZwUQwGxrlfvw7TuAAMLTWeo6C5jKhBUBj7yrE3v4Ug.uW.', 'bg1ReNVwaBsbAYp2Hp1fBlUNjYkEjNoUM8m1yI1gn7kMgNJsA6vSXyGQxU3R', '2019-02-03 20:43:58', '2019-02-03 20:43:58', 6),
(275, 'xy', 'MARIAMM', 'ADIAWIAKOYE', 'adiawiakoye.mariamm@club66-mali.com', '703156360', NULL, 'PRESI_1902040253280', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$5q0iKReBhAgFZ2Kz1GqKPukpaeweTijx5Y3AlygftmdVgu1u5E.X6', NULL, '2019-02-04 01:53:28', '2019-02-04 01:53:28', NULL),
(276, 'xy', 'ALIDOUABDOUL', 'AZIZ', 'aziz.alidouabdoul@club66-mali.com', '932322021', NULL, 'PRESI_1902040253281', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$X6NrKwKGHFZykWtm9May3eFhtmKETSGcpdGOwLS1YJYV381PuBSmq', NULL, '2019-02-04 01:53:28', '2019-02-04 01:53:28', NULL),
(277, 'xy', 'FATIM', 'BAMBA', 'bamba.fatim@club66-mali.com', '948259492', NULL, 'PRESI_1902040253282', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$tFa8nCe3MIsUMUxunm0EherFZ6BgRfGHXGJlxcMlWtS7xrWYoyO/C', NULL, '2019-02-04 01:53:28', '2019-02-04 01:53:28', NULL),
(278, 'xy', 'SOUMAILA', 'BOITE', 'boite.soumaila@club66-mali.com', '730121823', NULL, 'PRESI_1902040253283', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$rt7yISoW6V6ofOzy.60UVepb/CG08lVIfNMIz84CGJobNTYkZi6yG', NULL, '2019-02-04 01:53:28', '2019-02-04 01:53:28', NULL),
(279, 'xy', 'MODIBO', 'CISSE', 'cisse.modibo@club66-mali.com', '760292584', NULL, 'PRESI_1902040253284', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$9BUcMJ/2c2lGcSyJejtOKe2U1CUurvy0r/ucnfbmV.b/Fe4tlF6ly', NULL, '2019-02-04 01:53:28', '2019-02-04 01:53:28', NULL),
(280, 'xy', 'AMINATA', 'COULIBALY', 'coulibaly.aminata@club66-mali.com', '711323595', NULL, 'PRESI_1902040253285', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$.0g46qtUAsoMz9z/.S9x2.RYELEiYjaMTnSVfm8OGOdRDxbwKOFSG', NULL, '2019-02-04 01:53:29', '2019-02-04 01:53:29', NULL),
(281, 'xy', 'Aboubacar', 'MAIGA', 'maiga.aboubacar@club66-mali.com', '765309486', NULL, 'PRESI_1902040253296', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$Wq/nIeiDeqc9zgAbSMzB8.Ssmeophts.x7tmkv40VHC1tnTXHcpne', NULL, '2019-02-04 01:53:29', '2019-02-04 01:53:29', NULL),
(282, 'xy', 'OUMAR', 'DEMBELE', 'dembele.oumar@club66-mali.com', '657752527', NULL, 'PRESI_1902040253297', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$NAJSDdwve4SgdPTf97t9cuho29ARHidbVzLIoaBz2eWDiIM3uZeR6', NULL, '2019-02-04 01:53:29', '2019-02-04 01:53:29', NULL),
(283, 'xy', 'MOUSSA', 'DEMBELE', 'dembele.moussa@club66-mali.com', '999244558', NULL, 'PRESI_1902040253298', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$BtdJsE0kIphy9DuI2mLqJOZ2/0rMj.P5XJnI1VzxxcVQ0qv.7gVzC', NULL, '2019-02-04 01:53:29', '2019-02-04 01:53:29', NULL),
(284, 'xy', 'ZAKARIA', 'DIARRA', 'diarra.zakaria@club66-mali.com', '662636039', NULL, 'PRESI_1902040253299', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$UyKwg/OsnI2MMEef3o9sGOP1j.00f/Y4MisbrpV0P1ISwbUTsPkLC', NULL, '2019-02-04 01:53:29', '2019-02-04 01:53:29', NULL),
(285, 'xy', 'MAHAMADOU', 'DJIRE', 'djire.mahamadou@club66-mali.com', '6665901310', NULL, 'PRESI_19020402532910', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$W6rNZKXbpSs7hNB/AAKjTOacH4ypLfVcFDvdumCTthq2FLcRPgTE.', NULL, '2019-02-04 01:53:29', '2019-02-04 01:53:29', NULL),
(286, 'xy', 'SARADIE', 'KANTE', 'kante.saradie@club66-mali.com', '9142238111', NULL, 'PRESI_19020402532911', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$uYQoyYaxgQxCeni0eXSryev.fSg9LQbHEiTVI4KoH6zmRmQ/c3bx2', NULL, '2019-02-04 01:53:29', '2019-02-04 01:53:29', NULL),
(287, 'xy', 'DIAKALIA', 'KASSE', 'kasse.diakalia@club66-mali.com', '7841819312', NULL, 'PRESI_19020402532912', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$Kj9IGXjudDu3F.W2J9xPAOzlpsGcGsu7TlcVhXnO2fkclehtPerYW', NULL, '2019-02-04 01:53:29', '2019-02-04 01:53:29', NULL),
(288, 'xy', 'ADAMA', 'KONATE', 'konate.adama@club66-mali.com', '7048613913', NULL, 'PRESI_19020402532913', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$vwy.KvgIUw8yl7iF3/5zJOi2QqcFfn7lFTignRlEAJVBJ/bW674Y.', NULL, '2019-02-04 01:53:29', '2019-02-04 01:53:29', NULL),
(289, 'xy', 'ALIOU', 'KOUMARE', 'koumare.aliou@club66-mali.com', '7750818014', NULL, 'PRESI_19020402532914', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$hJ4AmFthueglC6xh6WIPDuuTTLTTSyUfWuvSZHDuIMYh3eroIzhTW', NULL, '2019-02-04 01:53:29', '2019-02-04 01:53:29', NULL),
(290, 'xy', 'CHEICKOUMAR', 'NIARE', 'niare.cheickoumar@club66-mali.com', '7271238615', NULL, 'PRESI_19020402532915', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$lWJuTiMDGDnfEqnUp2AvguIaof65gH.9152OwSS9u8jv/9k3HU9aW', NULL, '2019-02-04 01:53:29', '2019-02-04 01:53:29', NULL),
(291, 'xy', 'ABDOU', 'SIDIBE', 'sidibe.abdou@club66-mali.com', '7669472916', NULL, 'PRESI_19020402533016', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$Es6QdmV74yS4oEk4I8efPu1u3exg8ogsTNrG2RqRYcKhyhg7kC1pO', NULL, '2019-02-04 01:53:30', '2019-02-04 01:53:30', NULL),
(292, 'xy', 'SITA', 'SISSOKO', 'sissoko.sita@club66-mali.com', '7921054517', NULL, 'PRESI_19020402533017', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$S8cZHOGKh3hKuo508qg9veFkvNeE5TaNRyReZ1TthIzaNdqPEzM.6', NULL, '2019-02-04 01:53:30', '2019-02-04 01:53:30', NULL),
(293, 'xy', 'BEKAYE', 'SISSOKO', 'sissoko.bekaye@club66-mali.com', '7104034318', NULL, 'PRESI_19020402533018', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$ynHexXngVeafmtg7wWT9R.DRf30OrUhC/91b/PraDFQ67jdUgnOk2', NULL, '2019-02-04 01:53:30', '2019-02-04 01:53:30', NULL),
(294, 'xy', 'AMARA', 'SOUKOUNA', 'soukouna.amara@club66-mali.com', '7926480819', NULL, 'PRESI_19020402533019', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$6BhV0DyDxa7S/got0zQs/.iQXWfBII75T5jKHvYOUeP/nCy0btFMa', NULL, '2019-02-04 01:53:30', '2019-02-04 01:53:30', NULL),
(295, 'xy', 'ABDOULAZIZ', 'TOURE', 'toure.abdoulaziz@club66-mali.com', '7702784620', NULL, 'PRESI_19020402533020', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$HKJv6J/VKitoTtPzwvhLgudoJ6jHMVtixv37nWFJILkFlCBEdEPa2', NULL, '2019-02-04 01:53:30', '2019-02-04 01:53:30', NULL),
(296, 'xy', 'AMINATA', 'TRAORE', 'traore.aminata@club66-mali.com', '7311974321', NULL, 'PRESI_19020402533021', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$3izaBFuudCENkoywFB3g1un9mdGNujGqx7eywSenaJDS/3rA740M2', NULL, '2019-02-04 01:53:30', '2019-02-04 01:53:30', NULL),
(297, 'xy', 'AdjaKorotim', 'TRAORE', 'traore.adjakorotim@club66-mali.com', '7050501522', NULL, 'PRESI_19020402533022', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$lvtOQIvTb3nn9Ddh/E4J0.Plfr/ooy/aC5HZKA9.pE.tuhd1Ghe3.', NULL, '2019-02-04 01:53:30', '2019-02-04 01:53:30', NULL),
(298, 'xy', 'MAMADOU', 'COULIBALY', 'coulibaly.mamadou@club66-mali.com', '9977348923', NULL, 'PRESI_19020402533023', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$5HcVe5oCxXGfxWdfvM7ENOcMydw.kGNiaePTQ8bw9NL1Rl6tz61SS', NULL, '2019-02-04 01:53:30', '2019-02-04 01:53:30', NULL),
(299, 'xy', 'FATOUMATA', 'DRAMERA', 'dramera.fatoumata@club66-mali.com', '7776281524', NULL, 'PRESI_19020402533024', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$//b4niAbDQJ0dh7OIVmsyOsVu3SqBkb2i838IUP1ZE6c5oGoIWDv.', NULL, '2019-02-04 01:53:30', '2019-02-04 01:53:30', NULL),
(300, 'xy', 'ASSITA', 'HAIDARA', 'haidara.assita@club66-mali.com', '7881420225', NULL, 'PRESI_19020402533025', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$Bt7ojRB.6lc7h6t8dDnzdesS3mfQWUBTvgFRcgk21vLsHKpLdQfMi', NULL, '2019-02-04 01:53:30', '2019-02-04 01:53:30', NULL),
(301, 'xy', 'MALLET', 'KEITA', 'keita.mallet@club66-mali.com', '7509515726', NULL, 'PRESI_19020402533026', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$qzWoYYS6TW4Qb0H4vMtqee/7l2sCD.3uKxb9.mvvyyUTbW/DYKr8i', NULL, '2019-02-04 01:53:30', '2019-02-04 01:53:30', NULL),
(302, 'xy', 'YAYA', 'TOURE', 'toure.yaya@club66-mali.com', '7979755927', NULL, 'PRESI_19020402533127', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$twErsz3PlBaWJ1dGptq5IO618f/W9MvmIODUEDPfpZ.vIAzZUd9Q2', NULL, '2019-02-04 01:53:31', '2019-02-04 01:53:31', NULL),
(303, 'xy', 'RAMATOU', 'TOURE', 'toure.ramatou@club66-mali.com', '6675312628', NULL, 'PRESI_19020402533128', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$CQFC2omhBdA.00pbKJdw4eu.1b4Ys3OyIml2BLeOrqqCJgBPbISVy', NULL, '2019-02-04 01:53:31', '2019-02-04 01:53:31', NULL),
(304, 'xy', 'MOHAMEDCHERIF', 'HAIDARA', 'haidara.mohamedcherif@club66-mali.com', '7713499929', NULL, 'PRESI_19020402533129', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$zQh0v37Aqy3wXXIp5pnFxeMWOdwKevNuPzkLpU6mngw.WGfDgaRjW', NULL, '2019-02-04 01:53:31', '2019-02-04 01:53:31', NULL),
(305, 'xy', 'HAMIDOU', 'TRAORE', 'traore.hamidou@club66-mali.com', '7651067930', NULL, 'PRESI_19020402533130', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$mpHE27wkIbtQh4XTECytT.0JMvHmMuknmYmz7UZfy4PKVjuUzvkQS', NULL, '2019-02-04 01:53:31', '2019-02-04 01:53:31', NULL),
(306, 'xy', 'MOUSSA', 'KEITA', 'keita.moussa@club66-mali.com', '7670305931', NULL, 'PRESI_19020402533131', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$yhuku0A35UNuKFWWn.kCSOFY6ceKKoyGJMrXOVu4tkRoJGGuzlUGy', NULL, '2019-02-04 01:53:31', '2019-02-04 01:53:31', NULL),
(307, 'xy', 'MONIQUE', 'DENA', 'dena.monique@club66-mali.com', '7914487532', NULL, 'PRESI_19020402533132', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$oyYYqg5bWObiXtZLUkCMU.g7WqpJ4oMS87YIHcmtDvt3eXCC8j1.G', NULL, '2019-02-04 01:53:31', '2019-02-04 01:53:31', NULL),
(308, 'xy', 'MOUSSA', 'SACKO', 'sacko.moussa@club66-mali.com', '7451603533', NULL, 'PRESI_19020402533133', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$1rb1l2ihgQadYzmS7FjokOOn9SXx5UV5UQ5VrxR4NvO9w1EcMZWgC', NULL, '2019-02-04 01:53:31', '2019-02-04 01:53:31', NULL),
(309, 'xy', 'CHEICKNA', 'TOUNKARA', 'tounkara.cheickna@club66-mali.com', '7931336034', NULL, 'PRESI_19020402533134', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$jNIplFXiE6aFADIU113uz.n6Q9y2aZtSrdjqg/o1ULjp3Cw0WsKiu', NULL, '2019-02-04 01:53:31', '2019-02-04 01:53:31', NULL),
(310, 'xy', 'ALY', 'TOGOLA', 'togola.aly@club66-mali.com', '7509515735', NULL, 'PRESI_19020402533135', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$2TTXhYDnFz6tymhwV3QbZudVfLSKczVwc3iibYuspHhR4zZj/5Kxi', NULL, '2019-02-04 01:53:31', '2019-02-04 01:53:31', NULL),
(311, 'xy', 'MOHAMED', 'KAGNASSY', 'kagnassy.mohamed@club66-mali.com', '7467134436', NULL, 'PRESI_19020402533136', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$hWeRbkZ8XZwl7B15lu0QL.9gXCFNJrl5Us2tSWvLy0/I.QkIF6qia', NULL, '2019-02-04 01:53:31', '2019-02-04 01:53:31', NULL),
(312, 'xy', 'MOHAMED', 'DOUMBIA', 'doumbia.mohamed@club66-mali.com', '5050529237', NULL, 'PRESI_19020402533137', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$gvjOoaq7yJb57fxZ0tMSS.i7euBZW37JAY7sKPfBfu9Uj2fI8CVLa', NULL, '2019-02-04 01:53:32', '2019-02-04 01:53:32', NULL),
(313, 'xy', 'MARIAM', 'TRAORE', 'traore.mariam@club66-mali.com', '9325178038', NULL, 'PRESI_19020402533238', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$BHWqARdeFkjDWx8ZTiFKHOIX0RLx7JUa5gRkIM5VtexhaHHRYO0oi', NULL, '2019-02-04 01:53:32', '2019-02-04 01:53:32', NULL),
(314, 'xy', 'FATOUMATA', 'MAHAMADOU', 'mahamadou.fatoumata@club66-mali.com', '7080802039', NULL, 'PRESI_19020402533239', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$n4Nu8fqoD4hbVAMa0fHVfeAr1qPDmZglAPZroKaNJJUPvHIhSeU7i', NULL, '2019-02-04 01:53:32', '2019-02-04 01:53:32', NULL),
(315, 'xy', 'SALIMATOU', 'COULIBALY', 'coulibaly.salimatou@club66-mali.com', '6988300640', NULL, 'PRESI_19020402533240', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$DJ3TvfXWUuwfvrmuGVHwQuKUwxa7JSQcNGG3JJ1/PG8olKlR5mG1a', NULL, '2019-02-04 01:53:32', '2019-02-04 01:53:32', NULL),
(316, 'xy', 'YASSASALIF', 'DIAKITE', 'diakite.yassasalif@club66-mali.com', '7208221641', NULL, 'PRESI_19020402533241', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$cGgjxAWH5Su7kWQ03QU1kO0wGrWcWnavf3Bs8oyvQghCluWlS2EBG', NULL, '2019-02-04 01:53:32', '2019-02-04 01:53:32', NULL),
(317, 'xy', 'RAMATOULAYE', 'DIAKITE', 'diakite.ramatoulaye@club66-mali.com', '9097435342', NULL, 'PRESI_19020402533242', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$Pb8yEeRUjSqeOAT8liFNFe72r5DGw3gltQsc7i3io4.YKcDq4ZSqO', NULL, '2019-02-04 01:53:32', '2019-02-04 01:53:32', NULL),
(318, 'xy', 'JUSTIN', 'DIARRA', 'diarra.justin@club66-mali.com', '7628571843', NULL, 'PRESI_19020402533243', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$uKcEys89jvWwywQwGtJqPuLKJAF7yOI2oVuobh1YljoLwz7GjrR1y', NULL, '2019-02-04 01:53:32', '2019-02-04 01:53:32', NULL),
(319, 'xy', 'LISSA', 'KEITA', 'keita.lissa@club66-mali.com', '7702955544', NULL, 'PRESI_19020402533244', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$orinolckBj3sCdd/T2qgRuR0c7Qd65hMZs1Gel3uljyr3A7D6o1tC', NULL, '2019-02-04 01:53:32', '2019-02-04 01:53:32', NULL),
(320, 'xy', 'ISSIAKA', 'DIARRA', 'diarra.issiaka@club66-mali.com', '7918583545', NULL, 'PRESI_19020402533245', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$7WS7cd06UgwITccUM2Lcku5FPj4tXdmdn0c5aucoWywBzTjH.M2VK', NULL, '2019-02-04 01:53:32', '2019-02-04 01:53:32', NULL),
(321, 'xy', 'TIDIANI', 'DEMBELE', 'dembele.tidiani@club66-mali.com', '7931277446', NULL, 'PRESI_19020402533246', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$n2sPWZJCFtKddrMUXrrTX.xARy2zGeQMISw4RNGjeSaOHoebU6bWy', NULL, '2019-02-04 01:53:32', '2019-02-04 01:53:32', NULL),
(322, 'xy', 'Souleymane', 'BAGAYOKO', 'bagayoko.souleymane@club66-mali.com', '7527049947', NULL, 'PRESI_19020402533247', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$miKwGBwdpa9EoNc0Wwcdi.h2mDZ1AKd6sTlHrvtWid9WxtAyL6OQO', NULL, '2019-02-04 01:53:32', '2019-02-04 01:53:32', NULL),
(323, 'xy', 'Oumou', 'COULIBALY', 'coulibaly.oumou@club66-mali.com', '7171081148', NULL, 'PRESI_19020402533248', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$N/QqWrAj8ICwN1NCL/mF1ukvEo3XeFqswLj/iYYx.hNy/ffU3iCl2', NULL, '2019-02-04 01:53:33', '2019-02-04 01:53:33', NULL),
(324, 'xy', 'Sinaly', 'SOUNTOURA', 'sountoura.sinaly@club66-mali.com', '7573561449', NULL, 'PRESI_19020402533349', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$aJSm.R8DnJWgcBh6FVNyK.EsrdVTS6T.aVZ/f/8CG9R1hmxzCZOY6', NULL, '2019-02-04 01:53:33', '2019-02-04 01:53:33', NULL),
(325, 'xy', 'FATOUMATA', 'BERTHE', 'berthe.fatoumata@club66-mali.com', '6685689550', NULL, 'PRESI_19020402533350', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$kmHp/sMYeoknBRjD/Q.eUuNGdVFooOzCCgD7IFoSsbIm8SHGobWES', NULL, '2019-02-04 01:53:33', '2019-02-04 01:53:33', NULL),
(326, 'xy', 'Zeinabou', 'MAIGA', 'maiga.zeinabou@club66-mali.com', '7983299151', NULL, 'PRESI_19020402533351', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$g4miNVo2P161dBdZyWJNIuOOBUIzMRhrwwzMu6EVH4wgzFIcGqPh.', NULL, '2019-02-04 01:53:33', '2019-02-04 01:53:33', NULL),
(327, 'xy', 'Adama', 'DJIRE', 'djire.adama@club66-mali.com', '6964849752', NULL, 'PRESI_19020402533352', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$agoHIlWr..8mQYWzVABCW.00WbOFgmzld00Jadw2MJbVicMvpzcye', NULL, '2019-02-04 01:53:33', '2019-02-04 01:53:33', NULL),
(328, 'xy', 'Arouna', 'TRAORE', 'traore.arouna@club66-mali.com', '7637503353', NULL, 'PRESI_19020402533353', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$lsA/HJHZkJYe0FKWzXPdD.99R.Y5ZUraeyTefI2L/CEPSJCF4ZqDS', NULL, '2019-02-04 01:53:33', '2019-02-04 01:53:33', NULL),
(329, 'xy', 'Marc', 'DAKOUO', 'dakouo.marc@club66-mali.com', '7001510154', NULL, 'PRESI_19020402533354', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$2ZiuoQ59leL1yukLy.zmQ.iD4ixq8ZPCShcGbDjojLZuPj/jFpT/.', NULL, '2019-02-04 01:53:33', '2019-02-04 01:53:33', NULL),
(330, 'xy', 'ABDOULAZIZ', 'FANE', 'fane.abdoulaziz@club66-mali.com', '7339375955', NULL, 'PRESI_19020402533355', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$OVd9.DQzgRPLjlr8dd13J.PcJpJvTvVheUMMta7iNNb78.TcVy5jO', NULL, '2019-02-04 01:53:33', '2019-02-04 01:53:33', NULL),
(331, 'xy', 'ABDOULAYE', 'SIDIBE', 'sidibe.abdoulaye@club66-mali.com', '7779560456', NULL, 'PRESI_19020402533356', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$MRO/dtcV6DFrG9tSF.YWA.LE4UDXwjDJxBLtBg/sTGItaHPRAajDm', NULL, '2019-02-04 01:53:33', '2019-02-04 01:53:33', NULL),
(332, 'xy', 'Djegui', 'DOUCOURE', 'doucoure.djegui@club66-mali.com', '8347327157', NULL, 'PRESI_19020402533357', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$E4ZVw5zm029mghQiKEbM2OgCInI/PHwo73Q/sel3hUy4aSGKlfYZm', NULL, '2019-02-04 01:53:33', '2019-02-04 01:53:33', NULL),
(333, 'xy', 'Zakiatou', 'WALLET', 'wallet.zakiatou@club66-mali.com', '8325263058', NULL, 'PRESI_19020402533358', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$8ILiSrQXEl4sIfKSNR0ACuyhZO9uJFsTGloQnccoX0Wvy7UefTgFO', NULL, '2019-02-04 01:53:33', '2019-02-04 01:53:33', NULL),
(334, 'xy', 'Aichata', 'TRAORE', 'traore.aichata@club66-mali.com', '9312323859', NULL, 'PRESI_19020402533359', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$Ii9r4i0bNVm8Wqn84y9G.eEpY4ANneE7XaMZgPZA9n5Vwj3Vns.X.', NULL, '2019-02-04 01:53:34', '2019-02-04 01:53:34', NULL),
(335, 'xy', 'Aboubacar', 'KEITA', 'keita.aboubacar@club66-mali.com', '7002097360', NULL, 'PRESI_19020402533460', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$Fn6aZ61HWEJPCW7B4NyCiuJn6rWcefp0lOqMjcyvGdFfEhp73OJEu', NULL, '2019-02-04 01:53:34', '2019-02-04 01:53:34', NULL),
(336, 'xy', 'Assitan', 'DIARRA', 'diarra.assitan@club66-mali.com', '7291125061', NULL, 'PRESI_19020402533461', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$qKCsboz8/OxNaNtS/PGe9uYAY95RearihZoL./XODkflUxuB5xKgm', NULL, '2019-02-04 01:53:34', '2019-02-04 01:53:34', NULL),
(337, 'xy', 'Rokiatou', 'TOURE', 'toure.rokiatou@club66-mali.com', '7644180662', NULL, 'PRESI_19020402533462', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$8SnDReMIDDYFAY44O5TWhOu3vswZ0knPiBg3VvLccSUQjlaVl.es6', NULL, '2019-02-04 01:53:34', '2019-02-04 01:53:34', NULL),
(338, 'xy', 'Moussa', 'KONE', 'kone.moussa@club66-mali.com', '9246777163', NULL, 'PRESI_19020402533463', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$GlEGOTvrHuPLjze2eFgHoOp8tUBnw3JekwKl6AJARvivEW18aSp9K', NULL, '2019-02-04 01:53:34', '2019-02-04 01:53:34', NULL),
(339, 'xy', 'Penda', 'SOW', 'sow.penda@club66-mali.com', '9925664664', NULL, 'PRESI_19020402533464', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$Fgx0qrCwMQcFEzL7UMkEDuACG9neAePHb0jlaQpxqMoRGaLCQylsC', NULL, '2019-02-04 01:53:34', '2019-02-04 01:53:34', NULL),
(340, 'xy', 'Malick', 'N\'DIAYE', 'n\'diaye.malick@club66-mali.com', '7676464365', NULL, 'PRESI_19020402533465', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$pUchhupnRX/tabq0IIguXOI9uAppH6Lxo/C/D.CcsrBajdyb2L4hy', NULL, '2019-02-04 01:53:34', '2019-02-04 01:53:34', NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `abonnements`
--
ALTER TABLE `abonnements`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `agences`
--
ALTER TABLE `agences`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `centresinterets`
--
ALTER TABLE `centresinterets`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `communes`
--
ALTER TABLE `communes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `cotisations`
--
ALTER TABLE `cotisations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Index pour la table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Index pour la table `quartiers`
--
ALTER TABLE `quartiers`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `recrutements`
--
ALTER TABLE `recrutements`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Index pour la table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_telephone_unique` (`telephone`),
  ADD UNIQUE KEY `users_num_abonne_unique` (`num_abonne`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `abonnements`
--
ALTER TABLE `abonnements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `agences`
--
ALTER TABLE `agences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `centresinterets`
--
ALTER TABLE `centresinterets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=335;

--
-- AUTO_INCREMENT pour la table `communes`
--
ALTER TABLE `communes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `cotisations`
--
ALTER TABLE `cotisations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `quartiers`
--
ALTER TABLE `quartiers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT pour la table `recrutements`
--
ALTER TABLE `recrutements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `regions`
--
ALTER TABLE `regions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=341;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
