<?php

Route::get('/', array('name' => 'welcome', 'uses' => function () { return view('welcome'); }))->name('welcome');
Route::post('/login_user', 'UserController@loginUser')->name('login_user');
Route::post('/register_user', 'UserController@registerUser')->name('register_user');
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/user/activate_user/{email}/{activation_code}', 'UserController@activate_user')->name('activate_user');
Auth::routes();

Route::resource('recrutements', 'RecrutementControler');
Route::post('updateMembre','RecrutementControler@updateMembre')->name('updateMembre');

Route::get('/history_client', 'ClientController@history_client')->name('history_client');
Route::get('/payment_client', 'ClientController@payment_client')->name('payment_client');
Route::get('/recus_client', 'ClientController@recus_client')->name('recus_client');
Route::get('/inbox_client', 'ClientController@inbox_client')->name('inbox_client');
Route::get('/reminder_client', 'ClientController@reminder_client')->name('reminder_client');
Route::get('/tirage_client', 'ClientController@tirage_client')->name('tirage_client');
Route::post('select-ajax', ['as'=>'select-ajax','uses'=>'AjaxDemoController@selectAjax']);
Route::resource('abonnements',"AbonnementController");
Route::get('/logout', function () {
  Auth::logout();
  return Redirect::to('/');
});

//ADMIN
Route::get('manage_agences','AdminController@manage_agences')->name('manage_agences');
Route::post('create_agence','AdminController@create_agence')->name('create_agence');
Route::get('manage_clients','AdminController@manage_clients')->name('manage_clients');

Route::get('manage_collaborateurs','AdminController@manage_collaborateurs')->name('manage_collaborateurs');
Route::post('createCollaborateur','CollaborateurController@createCollaborateur')->name('createCollaborateur');
Route::post('updateCollaborateur','CollaborateurController@updateCollaborateur')->name('updateCollaborateur');

Route::get('manage_superviseurs','AdminController@manage_superviseurs')->name('manage_superviseurs');
Route::post('createSuperviseur','SuperviseurController@createSuperviseur')->name('createSuperviseur');
Route::post('updateSuperviseur','SuperviseurController@updateSuperviseur')->name('updateSuperviseur');


Route::get('manage_hyperviseurs','AdminController@manage_hyperviseurs')->name('manage_hyperviseurs');
Route::post('createHyperperviseur','HyperviseurController@createHyperperviseur')->name('createHyperperviseur');

Route::get('manage_recruteurs','AdminController@manage_recruteurs')->name('manage_recruteurs');
Route::post('createRecruteur','RecrutementControler@createRecruteur')->name('createRecruteur');
Route::post('updateRecruteur','RecrutementControler@updateRecruteur')->name('updateRecruteur');

Route::get('manage_admins','AdminController@manage_admins')->name('manage_admins');
Route::get('manage_role_perms','AdminController@manage_role_perms')->name('manage_role_perms');

//Collaborateur
Route::get('get_membres','CollaborateurController@manage_clients')->name('get_membres');
Route::post('post_payment','CotisationController@post_payment')->name('post_payment');
Route::post('load_member_for_payment','CollaborateurController@load_member_for_payment')->name('load_member_for_payment');
Route::get('stats_transaction_byProfile','CotisationController@stats_transaction_byProfile')->name('stats_transaction_byProfile');
//Route::get('new_payment','CotisationController@new_payment')->name('new_payment');
//Route::post('/autocomplete','CotisationController@fetch_membre')->name('search_id_membre');

//SUPERVISEUR
Route::get('get_collaborateurs_by_agence','SuperviseurController@get_collaborateurs_by_agence')->name('get_collaborateurs_by_agence');
Route::get('stats_transaction_agence','SuperviseurController@stats_transaction_agence')->name('stats_transaction_agence');
Route::post('createCollaborateurSup','SuperviseurController@createCollaborateurSup')->name('createCollaborateurSup');


Route::get('/contrat',function(){
  $array_files = ['Club66_Contrat_Adhesion_Association.pdf','Formulaire_Préinscription.pdf'];
  $zipname = 'contrat_club66.zip';
  $zip = new ZipArchive;
  $zip->open($zipname,ZipArchive::CREATE);
  foreach ($array_files as $file) {
    $zip->addFile('pdfs/'.$file);
  }
  $zip->close();
  header('Content-Type: application/zip');
  header('Content-disposition: attachment; filename='.$zipname);
  header('Content-Length: ' . filesize($zipname));
  readfile($zipname);
});


Route::get('/test',function() {

$arrays = [
  "ADIAWIAKOYE_MARIAMM_70315636",
  "AZIZ_ALIDOUABDOUL_93232202",
  "BAMBA_FATIM_94825949",
  "BOITE_SOUMAILA_73012182",
  "CISSE_MODIBO_76029258",
  "COULIBALY_AMINATA_71132359",
  "MAIGA_Aboubacar_76530948",
  "DEMBELE_OUMAR_65775252",
  "DEMBELE_MOUSSA_99924455",
  "DIARRA_ZAKARIA_66263603",
  "DJIRE_MAHAMADOU_66659013",
  "KANTE_SARADIE_91422381",
  "KASSE_DIAKALIA_78418193",
  "KONATE_ADAMA_70486139",
  "KOUMARE_ALIOU_77508180",
  "NIARE_CHEICKOUMAR_72712386",
  "SIDIBE_ABDOU_76694729",
  "SISSOKO_SITA_79210545",
  "SISSOKO_BEKAYE_71040343",
  "SOUKOUNA_AMARA_79264808",
  "TOURE_ABDOULAZIZ_77027846",
  "TRAORE_AMINATA_73119743",
  "TRAORE_AdjaKorotim_70505015",
  "COULIBALY_MAMADOU_99773489",
  "DRAMERA_FATOUMATA_77762815",
  "HAIDARA_ASSITA_78814202",
  "KEITA_MALLET_75095157",
  "TOURE_YAYA_79797559",
  "TOURE_RAMATOU_66753126",
  "HAIDARA_MOHAMEDCHERIF_77134999",
  "TRAORE_HAMIDOU_76510679",
  "KEITA_MOUSSA_76703059",
  "DENA_MONIQUE_79144875",
  "SACKO_MOUSSA_74516035",
  "TOUNKARA_CHEICKNA_79313360",
  "TOGOLA_ALY_75095157",
  "KAGNASSY_MOHAMED_74671344",
  "DOUMBIA_MOHAMED_50505292",
  "TRAORE_MARIAM_93251780",
  "MAHAMADOU_FATOUMATA_70808020",
  "COULIBALY_SALIMATOU_69883006",
  "DIAKITE_YASSASALIF_72082216",
  "DIAKITE_RAMATOULAYE_90974353",
  "DIARRA_JUSTIN_76285718",
  "KEITA_LISSA_77029555",
  "DIARRA_ISSIAKA_79185835",
  "DEMBELE_TIDIANI_79312774",
  "BAGAYOKO_Souleymane_75270499",
  "COULIBALY_Oumou_71710811",
  "SOUNTOURA_Sinaly_75735614",
  "BERTHE_FATOUMATA_66856895",
  "MAIGA_Zeinabou_79832991",
  "DJIRE_Adama_69648497",
  "TRAORE_Arouna_76375033",
  "DAKOUO_Marc_70015101",
  "FANE_ABDOULAZIZ_73393759",
  "SIDIBE_ABDOULAYE_77795604",
  "DOUCOURE_Djegui_83473271",
  "WALLET_Zakiatou_83252630",
  "TRAORE_Aichata_93123238",
  "KEITA_Aboubacar_70020973",
  "DIARRA_Assitan_72911250",
  "TOURE_Rokiatou_76441806",
  "KONE_Moussa_92467771",
  "SOW_Penda_99256646",
  "N'DIAYE_Malick_76764643"
];

  foreach($arrays as $key=>$tabs){
    $tab = explode("_", $tabs);
    DB::beginTransaction();
    try {
      $num_abonne = "PRESI_".date('ymdmis').$key;
      $user = \App\User::create([
          'civilite' => 'xy',
          'nom' => $tab[1],
          'prenom' => $tab[0],
          'email' => strtolower($tab[0].'.'.$tab[1].'@club66-mali.com'),
          'telephone' => $tab[2].$key,
          'telephone2' => null,
          'datedenaisssance' => null,
          'commune_id' => null,
          'quartier_id' => null,
          'adresse' => null,
          'password' => Hash::make("123456"),
          'num_abonne' => $num_abonne,
          'agence_id' => null,
          'status' => 1
      ]);
      if($user) {
        $role_president = \App\Role::findOrFail(6);
        $user->attachRole($role_president);
        DB::commit();
      }
    }catch(\Exception $e){
      DB::rollback();
      echo $e->getMessage();
    }
  }

//  return \App\Commune::with('agence')->find(1)->agence;
//return \App\User::find(94)->commune->id;
//  echo '<pre>';
//  print_r(auth()->user()->agence->id);
//return bcrypt('123456');
//return date("d-m-Y H:i:s");
//$now = \Carbon\Carbon::now();  echo $now;
//return bcrypt("passer123");
//$user = \App\User::where('email', '=', 'supclub66@gmail.com')->first();
//$role = \App\Role::findOrFail(3);
//$user->attachRole($role);
//$user1 = \App\User::where('email', '=', 'colaclub66@gmail.com')->first();
//$role1 = \App\Role::findOrFail(2);
//$user1->attachRole($role1);
//$user2 = \App\User::where('email', '=', 'hypclub66@gmail.com')->first();
//$role2 = \App\Role::findOrFail(4);
//$user2->attachRole($role2);
///$user3 = \App\User::where('email', '=', 'adminclub66@gmail.com')->first();
///$role3 = \App\Role::findOrFail(5);
//$user3->attachRole($role3);
});



Route::get('/test2',function() {

  $arrays = [
      "NIENTAO_Seydou_75325030",
      "DOUCOURE_Amadou_77235036",
      "DIAKITE_SalahSalif_74539792",
      "COULIBALY_Mohamed_74306362"
  ];

  foreach($arrays as $key=>$tabs){
    $tab = explode("_", $tabs);
    DB::beginTransaction();
    try {
      $num_abonne = "PRESI_".date('ymdmis').$key;
      $user = \App\User::create([
          'civilite' => 'xy',
          'nom' => $tab[1],
          'prenom' => $tab[0],
          'email' => strtolower($tab[0].'.'.$tab[1].'@club66-mali.com'),
          'telephone' => $tab[2].$key,
          'telephone2' => null,
          'datedenaisssance' => null,
          'commune_id' => null,
          'quartier_id' => null,
          'adresse' => null,
          'password' => Hash::make("123456"),
          'num_abonne' => $num_abonne,
          'agence_id' => null,
          'status' => 1
      ]);
      if($user) {
        $role_sup = \App\Role::findOrFail(3);
        $user->attachRole($role_sup);
        DB::commit();
      }
    }catch(\Exception $e){
      DB::rollback();
      echo $e->getMessage();
    }
  }

});

