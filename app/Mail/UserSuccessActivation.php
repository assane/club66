<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserSuccessActivation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * UserSuccessActivation constructor.
     */
    public function __construct()
    {

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'Activation effectuée avec succès';
        return $this->markdown('backend.mails.user.usersuccessactivation')->subject($subject);
    }
}
