<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewUserWelcome extends Mailable
{
    use Queueable, SerializesModels;
    public $inputs;
    public function __construct($inputs)
    {
        $this->inputs = $inputs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//      return $this->markdown('backend.mails.user.newuserwelcome');
        $subject = 'Activation Compte Club 66';
        return $this->markdown('backend.mails.user.newuserwelcome')->subject($subject)
                    ->with(['inputs' => $this->inputs]);
    }
}
