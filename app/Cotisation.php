<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cotisation extends Model {
    
    public $fillable = [
        'numero_transaction',
        'montant',
        'client_id',
        'user_id',
        'agence_id',
        'date_depot',
        'centresinteret_id',
        'mois_cotisation'
    ];

    public $timestamps = false;

    public function client(){
        return $this->belongsTo(User::class);
    }

    public function agence(){
        return $this->belongsTo(Agence::class);
    }

    public function centresinteret(){
        return $this->belongsTo(Centresinteret::class);
    }
}
