<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recrutement extends Model {


    protected $fillable = [
        'user_id',
        'agent_id',
        'date_recrutement'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function agent(){
        return $this->belongsTo('App\User','agent_id');
    }
}
