<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agence extends Model {

    public $fillable = [
        'nom_agence',
        'commune_id'
    ];

    public $timestamps = false;

    public function commune () {
        return $this->belongsTo('App\Commune');
    }
}


