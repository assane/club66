<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom','prenom', 'email', 'password','telephone','num_abonne','status',
        'datedenaisssance','nationalite','adresse','commune_id','quartier_id','civilite',
        'agence_id'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function centres_interets() {
        return $this->belongsToMany(Centresinteret::class);
    }

    public function commune(){
        return $this->belongsTo(Commune::class);
    }

    public function quartier(){
        return $this->belongsTo(Quartier::class);
    }

    public function agence(){
        return $this->belongsTo(Agence::class);
    }


    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
}
