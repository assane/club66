<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Abonnement extends Model
{
    //
    protected $fillable = [
        'numero_transaction',
        'montant',
        'client_id',
        'user_id',
        'date_abonnement',
        'agence_id',
        'annee_abonnement',
    ];

    public $timestamps = false;
}
