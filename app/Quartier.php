<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quartier extends Model {

    protected $fillable = [
        'quartier',
        'commune_id',
    ];

    public $timestamps = false;

    public function commune(){
        return $this->belongsTo(Commune::class);
    }
}
