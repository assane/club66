<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commune extends Model
{
    protected $fillable = [
        'commune'
    ];

    public function agence(){
        return $this->hasMany('\App\Agence');
    }
}
