<?php

namespace App\Http\Controllers;

use App\Http\Requests\InscriptionRequest;
use App\Http\Requests\LoginRequest;
use App\Mail\NewUserWelcome;
use App\Mail\UserSuccessActivation;
use App\User;
//use Illuminate\Support\Facades\Auth;
use Auth;
use DB;
use Illuminate\Support\Facades\Hash;
use Mail;
use Twilio\Rest\Client;

class UserController extends Controller {
	/**
	 * UserController constructor.
	 */
	public function __construct() {
		$this->middleware('guest');
	}

	public function registerUser(InscriptionRequest $data) {
		$num_abonne = strtoupper(substr($data['prenom'], 0, 3)) . date('ymdmis') . strtoupper(substr($data['prenom'], -3));
		$hashed_random_password = str_random(8);
		$user = User::create([
			'civilite' => $data['civilite'],
			'nom' => trim($data['nom']),
			'prenom' => trim($data['prenom']),
			'email' => trim($data['email']),
			'telephone' => trim($data['telephone']),
			'telephone2' => trim($data->telephone2),
			'adresse' => $data['adresse'],
			'datedenaisssance' => $data['datedenaissance'],
			'commune_id' => $data['commune_id'],
			'quartier_id' => $data['quartiers'],
			'password' => Hash::make($hashed_random_password),
			'num_abonne' => $num_abonne
		]);
		if ($user) {
			$activation_code = base64_encode(substr($data->email, 0, 4));
			$this->sendEmailActivation($user->email, [
//                'activation_code' => base64_encode($data->email),
				'activation_code' => $activation_code,
				'nom' => $data->nom,
				'prenom' => $data->prenom,
				'email' => $data->email,
                'telephone' => $data->telephone,
                'num_abonne' => $num_abonne,
                'mot_de_passe' => $hashed_random_password,
				'link' => route('activate_user', array('email' => $data->email, 'activation_code' => $activation_code)),
			]);

			foreach($data->centres_interets as $val){
				DB::table('centresinteret_user')
						->insert(['user_id' => $user->id, 'centresinteret_id' => $val]
				);
			}

			foreach($data->centres_interets2 as $val){
				DB::table('centresinteret_user')
						->insert(['user_id' => $user->id, 'centresinteret_id' => $val]
						);
			}

			$role_membre = \App\Role::findOrFail(1);
			$user->attachRole($role_membre);

			//$this->sendSMSActivation($data->telephone, $activation_code, $num_abonne);
            // $this->sendSMSActivation_Nexmo($data->telephone, $activation_code, $num_abonne);
            return response()->json([
				'status' => 'success',
				'message' => 'Inscription réussie avec succès',
				'num_abonne' => $num_abonne,
			]);
		} else {
			return response()->json([
				'status' => 'error',
				'message' => 'Votre inscription n\'a pas aboutie',
			]);
		}
	}

	public function loginUser(LoginRequest $request) {
		//if (auth()->attempt(array('email' => $request->input('login_user'), 'password' => $request->input('password_user')), true)) {
	//	if (Auth::attempt(array('email' => $request->input('login_user'), 'password' => $request->input('password_user')), true)) {
		if (Auth::attempt(['email' => $request->input('login_user'), 'password' => $request->input('password_user'), 'status' => 1])) {
			return response()->json([
				'status' => 'success',
				'message' => 'Authentification Réussie',
				'route' => route('dashboard'),
				'code_status' => 200
			]);
		} else {
			if(User::whereEmail($request->input('login_user'))->first()->status == 0){
				return response()->json([
						'status' =>  "error",
						'message' => "Compte inactive",
						'code_status' => 419
				]);
			}
			return response()->json([
				'status' => 'error',
				'message' => 'Votre login ou mot passe est incorrect',
        		'code_status' => 500
			]);
		}
	}

	public function sendEmailActivation($email, $inputs) {
		Mail::to($email)->send(new NewUserWelcome($inputs));
	}

	public function sendSMSActivation($tel, $activation_code, $num_abonne) {
		$account_sid = config('app.twilio')['TWILIO_ACCOUNT_SID'];
		$auth_token = config('app.twilio')['TWILIO_AUTH_TOKEN'];
		$twilio_number = config('app.twilio')['TWILIO_SMS_NUMBER'];
		$body = "Club 66 \n Bonjour, \n Inscription Reussie  \n Votre Numéro abonnement :" . $num_abonne . " \n Code d'activation :" . $activation_code;

		$client = new Client($account_sid, $auth_token);
		$client->messages->create(
			'+' . $tel,
			array(
				'from' => $twilio_number,
				'body' => $body,
			)
		);
	}
    
    public function sendSMSActivation_Nexmo($tel, $activation_code, $num_abonne){
        $body = "Club 66 \n Bonjour, \n Inscription Reussie  \n Votre Numéro abonnement :" . $num_abonne . " \n Code d'activation :" . $activation_code;
        $basic  = new \Nexmo\Client\Credentials\Basic('f2574af7', '32febb05f61bfe1b');
        $client = new \Nexmo\Client($basic);
        $message = $client->message()->send([
            'to' => $tel,
            'from' => 'CLUB 66',
            'text' => $body
        ]);
    }


	public function activate_user($email, $activationCode) {
		$user = User::whereEmail($email)->first();
		if (base64_encode(substr($user->email, 0, 4)) == $activationCode) {
			$user->activation_code = $activationCode;
			$user->status = 1;
			if ($user->save()) {
				Mail::to($email)->send(new UserSuccessActivation());
			}
		}
		return redirect()->route('welcome');
	}

	public function logout_user() {
		Auth::logout();
		return redirect('/');
	}

}
