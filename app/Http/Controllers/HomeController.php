<?php

namespace App\Http\Controllers;

use App\Centresinteret;
use Auth;

class HomeController extends Controller {
	/**
	 * HomeController constructor.
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index() {
		return view('backend.dashboard');
	}
}
