<?php

namespace App\Http\Controllers;

use App\Cotisation;
use App\Http\Requests\PostPaymentRequest;
use App\User;
use Illuminate\Http\Request;
use DB;

class CotisationController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function fetch_membre(Request $request){
        if($request->query){
            $query =  $request->get('get');
            $datas_members = User::whereHas('roles', function ($q) use ($query) {
                $q->where('name', 'client')
                  ->where('nom', 'like',"%{$query}%")
                  ->where('prenom', 'like',"%{$query}%");
            })->get();
            $output = "<ul class='dropdown-menu' style='display:block;position: relative'>";
            foreach($datas_members as $a_member){
                $output .= '<li><a href="#">'.$a_member->nom.' '.$a_member->prenom.'</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }


    public function post_payment(PostPaymentRequest $request){
        DB::beginTransaction();
        try {
            $numero_transaction = 'TR-'.date('mdymis').'-COT-'.strtoupper(str_random(5));
            $transaction = Cotisation::create([
                'numero_transaction' => $numero_transaction,
                'montant' => $request->montant,
                'client_id' => $request->client_id,
                'user_id' => auth()->user()->id,
                'mois_cotisation' => \Carbon\Carbon::now()->format("m/y"),
                'agence_id' => $request->agence_id,
                'date_depot' => \Carbon\Carbon::now(),
                'centresinteret_id' => $request->centresinteret_id
            ]);
            if($transaction) {
                DB::commit();
                return response()->json([
                    'code_status' => 200,
                    'message' => 'Transaction effectuée avec succès',
                    'status' => 'success',
                    'numero_transaction' => $numero_transaction
                ]);
            }
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'code_status' => 500,
                'message' => 'Opération non aboutie !',
                'status' => 'error',
                'message_error' => $e->getMessage()
            ]);
        }
    }

    public function stats_transaction_byProfile(){
      $stats_all_cotisation_by_profile =  Cotisation::where('user_id',auth()->user()->id)->get();
      return view('backend.stats_transaction_byProfile')->with(compact('stats_all_cotisation_by_profile'));
    }

    public static function get_total_paiement_by_agence_Col(){
       return \App\Cotisation::where('agence_id',auth()->user()->agence->id)->sum('montant');
   }

    public static function get_total_paiement_by_agence_Today_Col(){
      return  \App\Cotisation::where('agence_id',auth()->user()->agence->id)
                             ->whereBetween('date_depot', [\Carbon\Carbon::now()->startOfDay(), \Carbon\Carbon::now()->endOfDay()])
                             ->sum('montant');
    }

    public static function get_total_paiement_by_agence_Months_Col(){
      return  \App\Cotisation::where('agence_id',auth()->user()->agence->id)
                             ->whereBetween('date_depot', [\Carbon\Carbon::now()->startOfMonth(), \Carbon\Carbon::now()->endOfMonth()])
                             ->sum('montant');
    }

}
