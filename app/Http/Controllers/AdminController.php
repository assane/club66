<?php

namespace App\Http\Controllers;

use App\Agence;
use App\Http\Requests\AgenceRequest;
use App\Role;
use App\User;
use DB;

class AdminController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function manage_clients() {
        $membres = User::whereHas('roles', function($q){
            $q->where('name', 'client');
        })->get();
        return view('backend.admin.manage_clients')->with(compact('membres'));
    }

    public function manage_collaborateurs(){
        $collaborateurs = User::whereHas('roles', function($q){
            $q->where('name', 'collaborateur');
        })->get();
        return view('backend.admin.manage_collaborateurs')->with(compact('collaborateurs'));
    }

    public function manage_superviseurs() {
        $superviseurs = User::whereHas('roles', function($q){
            $q->where('name', 'superviseur');
        })->get();
        return view('backend.admin.manage_superviseurs')->with(compact('superviseurs'));
    }

    public function manage_hyperviseurs() {
        $hyperviseurs = User::whereHas('roles', function($q){
            $q->where('name', 'hyperviseur');
        })->get();
        return view('backend.admin.manage_hyperviseurs')->with(compact('hyperviseurs'));
    }

    public function manage_recruteurs() {
        $recruteurs = User::whereHas('roles', function($q){
            $q->where('name', 'president');
        })->get();
        return view('backend.admin.manage_recruteurs')->with(compact('recruteurs'));
    }

    public function manage_admins() {
        return view('backend.admin.manage_admins');
    }

    public function manage_role_perms() {
        $roles = Role::all();
        return view('backend.admin.manage_role_perms')->with(compact('roles'));
    }



    public function manage_agences() {
        $agences = Agence::all();
        return view('backend.admin.manage_agences')->with(compact('agences'));
    }

    public function create_agence(AgenceRequest $request) {
        DB::beginTransaction();
        try {
            $agence =  Agence::create([
               'nom_agence' => $request->nom_agence,
               'commune_id' => $request->commune_id,
            ]);
            if($agence){
                DB::commit();
                return response()->json([
                    'code_status' => 200,
                    'message' => 'Opération effectué avec succès',
                    'status' => 'success'
                ]);
            }
        }catch(\Exception $e){
            return response()->json([
                'code_status' => 500,
                'message' => 'Recrutement non aboutie !',
                'status' => 'error',
                'message_error' => $e->getMessage()
            ]);
            DB::rollback();
        }
    }

}
