<?php


namespace App\Http\Controllers;


use App\Commune;
use App\Quartier;
use Illuminate\Http\Request;
use DB;


class AjaxDemoController extends Controller
{
    /**
     * Show the application myform.
     *
     * @return \Illuminate\Http\Response
     */
    public function myform()
    {
        $countries = DB::table('countries')->pluck("name","id")->all();
        return view('myform',compact('countries'));
    }


    /**
     * Show the application selectAjax.
     *
     * @return \Illuminate\Http\Response
     */
    public function selectAjax(Request $request)
    {
       if($request->ajax()){
            $quartiers = DB::table('quartiers')->where('commune_id',$request->commune_id)->pluck("quartier","id")->all();
            $data = view('ajax-select',compact('quartiers'))->render();
            return response()->json(['options'=>$data]);
        }
    }
}