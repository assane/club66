<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClientController extends Controller {

    public function __construct() {
        $this->middleware(['auth']);
    }
    
    public function history_client() {
        return view('backend.client.history_client');
    }

    public function payment_client(){
        return view('backend.client.payment_client');
    }

    public function recus_client ()
    {
        return redirect()->route('dashboard');
    }

    public function inbox_client()
    {
        return view('backend.client.inbox_client');
    }

    public function reminder_client()
    {
        return redirect()->route('dashboard');
    }

    public function tirage_client()
    {
        return redirect()->route('dashboard');
    }
}
