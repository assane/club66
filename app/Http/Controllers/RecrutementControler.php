<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRecrutementRequest;
use App\Http\Requests\RecrutementRequest;
use App\Mail\NewUserWelcome;
use App\Recrutement;
use App\User;
use Illuminate\Http\Request;
use DB;
use Hash;
use Mail;

class RecrutementControler extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recrus = Recrutement::where('agent_id',auth()->user()->id)->get();
        return view('backend.president.index')->with(compact('recrus'));
    }

    /**
     * @param RecrutementRequest $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(RecrutementRequest $data)
    {
        DB::beginTransaction();
        try {

            $num_abonne = strtoupper(substr($data['prenom'], 0, 3)) . date('ymdmis') . strtoupper(substr($data['prenom'], -3));
            $hashed_random_password = str_random(8);
            $user = User::create([
                'civilite' => $data['civilite'],
                'nom' => $data['nom'],
                'prenom' => $data['prenom'],
                'email' => $data['email'],
                'telephone' => trim($data['telephone']),
                'telephone2' => trim($data->telephone2),
                'datedenaisssance' => $data['datedenaissance'],
                'commune_id' => $data['commune_id'],
                'quartier_id' => $data['quartiers'],
                'adresse' => $data['adresse'],
                'password' => Hash::make($hashed_random_password),
                'num_abonne' => $num_abonne,
                'status' => 1
            ]);

                if($user) {
                    DB::commit();
                    if(auth()->user()->hasRole('president')){
                        Recrutement::create([
                            'user_id' => $user->id,
                            'agent_id' => auth()->user()->id,
                            'date_recrutement' => \Carbon\Carbon::now()
                        ]);
                    }

                    $activation_code = base64_encode(substr($data->email, 0, 4));
                    $this->sendEmailActivation($user->email, [
//                  'activation_code' => base64_encode($data->email),
                        'activation_code' => $activation_code,
                        'nom' => $data->nom,
                        'prenom' => $data->prenom,
                        'email' => $data->email,
                        'telephone' => $data->telephone,
                        'num_abonne' => $num_abonne,
                        'mot_de_passe' => $hashed_random_password,
                        'link' => route('activate_user', array('email' => $data->email, 'activation_code' => $activation_code)),
                    ]);

                    $role_membre = \App\Role::findOrFail(1);
                    $user->attachRole($role_membre);

                    foreach($data->centres_interets as $val){
                        DB::table('centresinteret_user')
                            ->insert(['user_id' => $user->id, 'centresinteret_id' => $val]
                            );
                    }
                    foreach($data->centres_interets2 as $val){
                        DB::table('centresinteret_user')
                            ->insert(['user_id' => $user->id, 'centresinteret_id' => $val]
                            );
                    }
                    
                    return response()->json([
                        'code_status' => 200,
                        'message' => 'Recrutement effectué avec succès',
                        'status' => 'success',
                        'num_abonne' => $num_abonne,
                        'id_client' => $user->id
                    ]);
                }
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'code_status' => 500,
                'message' => 'Recrutement non aboutie !',
                'status' => 'error',
                'message_error' => $e->getMessage()
            ]);
        }
    }

    public function updateMembre(Request $request){


        $user = User::findOrFail($request->idUser);

        DB::beginTransaction();
        try {

           $check= $user->update([
                'civilite' => $request->editCivilite,
                'nom' => $request->editNom,
                'prenom' => $request->editPrenom,
                'email' => $request->editEmail,
                'telephone' => $request->editTelephone,
                'telephone2' => $request->editTelephone2,
                'datedenaisssance' => $request->editDatedenaissance,
                'commune_id' => $request->editCommune_id,
                'quartier_id' => $request->editQuartier_id,
                'adresse' => $request->editAdresse,
                'status' => $request->editStatus
            ]);
            if($check) {

                DB::commit();
                return response()->json([
                    'code_status' => 200,
                    'message' => 'Opération effectuée avec succès',
                    'status' => 'success',
                ]);
            }

           }
           catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'code_status' => 500,
                'message' => 'Recrutement non aboutie !',
                'status' => 'error',
                'message_error' => $e->getMessage()
            ]);
        }
    }

    public function sendEmailActivation($email, $inputs) {
        Mail::to($email)->send(new NewUserWelcome($inputs));
    }

    public function createRecruteur(CreateRecrutementRequest $data){
        DB::beginTransaction();
        try {
            $num_abonne = "PRESI_".date('ymdmis');
            $user = User::create([
                'civilite' => $data['civilite'],
                'nom' => $data['nom'],
                'prenom' => $data['prenom'],
                'email' => $data['email'],
                'telephone' => trim($data['telephone']),
                'telephone2' => null,
                'datedenaisssance' => null,
                'commune_id' => null,
                'quartier_id' => null,
                'adresse' => null,
                'password' => Hash::make($data['password']),
                'num_abonne' => $num_abonne,
                'agence_id' => null,
                'status' => 1
            ]);
            if($user) {
                $role_president = \App\Role::findOrFail(6);
                $user->attachRole($role_president);
                DB::commit();
                return response()->json([
                    'code_status' => 200,
                    'message' => 'Opération effectuée avec succès',
                    'status' => 'success',
                    'num_abonne' => $num_abonne
                ]);
            }
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'code_status' => 500,
                'message' => 'Opération non aboutie !',
                'status' => 'error',
                'message_error' => $e->getMessage()
            ]);
        }
    }

    public function updateRecruteur(Request $request)
    {

       $user = User::findOrFail($request->idUser);
       if ($request->ajax()) {
            DB::beginTransaction();
            try {

                $check =$user->update(['status' => $request->editStatus]);

                if ($check) {
                    DB::commit();

                    return response()->json([
                        'code_status' => 200,
                        'message' => 'Recruteur modifié avec succès',
                        'status' => 'success',
                    ]);
                }

            } catch (\Exception $e) {

                DB::rollback();
                return response()->json([
                    'code_status' => 500,
                    'message' => 'Opération non aboutie !',
                    'status' => 'error',
                    'message_error' => $e->getMessage()
                ]);
            }

        }
    }

}
