<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateHyperviseurRequest;
use App\User;
use DB;
use Illuminate\Support\Facades\Hash;


class HyperviseurController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * @param CreateHyperviseurRequest $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function createHyperperviseur(CreateHyperviseurRequest $data){
        DB::beginTransaction();
        try {
            $num_abonne = "HYPER_".date('ymdmis');
            $user = User::create([
                'civilite' => $data['civilite'],
                'nom' => $data['nom'],
                'prenom' => $data['prenom'],
                'email' => $data['email'],
                'telephone' => trim($data['telephone']),
                'telephone2' => null,
                'datedenaisssance' => null,
                'commune_id' => null,
                'quartier_id' => null,
                'adresse' => null,
                'password' => Hash::make($data['password']),
                'num_abonne' => $num_abonne,
                'agence_id' => $data['agence_id'],
                'status' => 1
            ]);
            if($user) {
                $role_hyperviseur = \App\Role::findOrFail(4);
                $user->attachRole($role_hyperviseur);
                DB::commit();
                return response()->json([
                    'code_status' => 200,
                    'message' => 'Opération effectuée avec succès',
                    'status' => 'success',
                    'num_abonne' => $num_abonne
                ]);
            }
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'code_status' => 500,
                'message' => 'Opération non aboutie !',
                'status' => 'error',
                'message_error' => $e->getMessage()
            ]);
        }
    }

    public static  function get_cclients_in_allAgence(){
       return User::whereHas('roles', function($q){
            $q->where('name', 'client');
        })->get()->count();
    }

    public static  function get_collaborateurs_in_allAgence(){
        return User::whereHas('roles', function($q){
            $q->where('name', 'collaborateur');
        })->get()->count();
    }

    public static  function get_superviseurs_in_allAgence(){
        return User::whereHas('roles', function($q){
            $q->where('name', 'superviseur');
        })->get()->count();
    }

    public static  function get_hyperviseurs_in_allAgence(){
        return User::whereHas('roles', function($q){
            $q->where('name', 'hyperviseur');
        })->get()->count();
    }


}
