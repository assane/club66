<?php

namespace App\Http\Controllers;

use App\Cotisation;
use App\Http\Requests\CreateCollaborateurSupRequest;
use App\Http\Requests\CreateSuperviseurRequest;
use App\User;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class SuperviseurController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**/
    public function createSuperviseur(CreateSuperviseurRequest $data){
        DB::beginTransaction();
        try {
            $num_abonne = "SUP_".date('ymdmis');
            $user = User::create([
                'civilite' => $data['civilite'],
                'nom' => $data['nom'],
                'prenom' => $data['prenom'],
                'email' => $data['email'],
                'telephone' => trim($data['telephone']),
                'telephone2' => null,
                'datedenaisssance' => null,
                'commune_id' => $data['commune_id'],
                'quartier_id' => null,
                'adresse' => null,
                'password' => Hash::make($data['password']),
                'num_abonne' => $num_abonne,
                'agence_id' => null,
                'status' => 1
            ]);
            if($user) {
                $role_superviseur = \App\Role::findOrFail(3);
                $user->attachRole($role_superviseur);
                DB::commit();
                return response()->json([
                    'code_status' => 200,
                    'message' => 'Opération effectuée avec succès',
                    'status' => 'success',
                    'num_abonne' => $num_abonne
                ]);
            }
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'code_status' => 500,
                'message' => 'Opération non aboutie !',
                'status' => 'error',
                'message_error' => $e->getMessage()
            ]);
        }
    }

    public function updateSuperviseur(Request $request){

        $user = User::findOrFail($request->idUser);
        if ($request->ajax()) {
            DB::beginTransaction();
            try {

                $check =$user->update(['status' => $request->editStatus,'commune_id'=> $request->editCommune_id]);

                if ($check) {
                    DB::commit();

                    return response()->json([
                        'code_status' => 200,
                        'message' => 'Superviseur modifié avec succès',
                        'status' => 'success',
                    ]);
                }

            } catch (\Exception $e) {

                DB::rollback();
                return response()->json([
                    'code_status' => 500,
                    'message' => 'Opération non aboutie !',
                    'status' => 'error',
                    'message_error' => $e->getMessage()
                ]);
            }

        }
    }


    public function get_collaborateurs_by_agence(){
        $tab_id = [];
        foreach (User::find(auth()->user()->id)->commune->agence as $item) {
            $tab_id [] = $item->id;
        }
        $collaborateurs = User::whereHas('roles', function ($query) use ($tab_id) {
            $query->where('name', 'collaborateur')
                  ->whereIn('agence_id', $tab_id );
        })->get();
        return view('backend.superviseur.manage_collaborateurs')->with(compact('collaborateurs'));
    }


    public function stats_transaction_agence(){
        $tab_id = [];
        foreach (\App\User::find(94)->commune->agence as $item) {
            $tab_id [] = $item->id;
        }
        $stats_all_cotisation_by_profile =  Cotisation::whereIn('agence_id',$tab_id)->get();
        return view('backend.stats_transaction_byProfile')->with(compact('stats_all_cotisation_by_profile'));
    }


    /**
     * @param CreateCollaborateurSupRequest $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function createCollaborateurSup(CreateCollaborateurSupRequest $data){
        DB::beginTransaction();
        try {
            $num_abonne = "COLAB_".date('ymdmis');
            $user = User::create([
                'civilite' => $data['civilite'],
                'nom' => $data['nom'],
                'prenom' => $data['prenom'],
                'email' => $data['email'],
                'telephone' => trim($data['telephone']),
                'telephone2' => null,
                'datedenaisssance' => null,
                'commune_id' => null,
                'quartier_id' => null,
                'adresse' => null,
                'password' => Hash::make($data['password']),
                'num_abonne' => $num_abonne,
                'agence_id' => auth()->user()->agence->id,
                'status' => 1
            ]);
            if($user) {
                $role_collaborateur = \App\Role::findOrFail(2);
                $user->attachRole($role_collaborateur);
                DB::commit();
                return response()->json([
                    'code_status' => 200,
                    'message' => 'Opération effectuée avec succès',
                    'status' => 'success',
                    'num_abonne' => $num_abonne
                ]);
            }
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'code_status' => 500,
                'message' => 'Opération non aboutie !',
                'status' => 'error',
                'message_error' => $e->getMessage()
            ]);
        }
    }

    public static function get_Solde_Commune_Sup(){
        $tab_id = [];
        foreach (\App\User::find(94)->commune->agence as $item) {
            $tab_id [] = $item->id;
        }
        return  \App\Cotisation::whereIn('agence_id',$tab_id)->sum('montant');
    }

    public static function get_Solde_Commune_Months_Sup(){
        $tab_id = [];
        foreach (\App\User::find(94)->commune->agence as $item) {
            $tab_id [] = $item->id;
        }
        return  \App\Cotisation::whereIn('agence_id',$tab_id)
            ->whereBetween('date_depot', [\Carbon\Carbon::now()->startOfMonth(), \Carbon\Carbon::now()->endOfMonth()])
            ->sum('montant');
    }

    public static function get_all_collaborateur_by_commune(){
        $commune_id = auth()->user()->commune->id;
        return  User::whereHas('roles', function ($query) use ($commune_id) {
            $query->where('name', 'collaborateur')
            ->where('commune_id', $commune_id);
        })->count();
    }

}
