<?php

namespace App\Http\Controllers;

use App\Abonnement;
use App\Http\Requests\AbonnementRequest;
use App\User;
use DB;
use Auth;

class AbonnementController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * @param AbonnementRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AbonnementRequest $request) {

            DB::beginTransaction();
            $numero_transaction = 'ABO_'.date('ymdmis').'_'.str_random(3);
            try {
                   $newAbonnement=Abonnement::create([
                        'numero_transaction' => $numero_transaction,
                        'montant' => $request->montantAbonnement,
                        'client_id' => $request->client_id,
                        'user_id' => Auth::user()->id,
//                        'date_abonnement' => date('d-m-Y m:i:s',strtotime($request->date_abonnement)),
                        'date_abonnement' => \Carbon\Carbon::now(),
                        'annee_abonnement' => $request->annee_abonnement,
                    ]);
                  if ($newAbonnement){
                      User::where('id',$request->client_id)->update(['abonnement_status' => 1]);
                      DB::commit();
                      return response()->json([
                          'code_status' => 200,
                          'message' => 'Opération effectuée avec succès',
                          'status' => 'success'
                      ]);
                  }
            } catch(\Exception $e){
                DB::rollBack();
                return response()->json([
                    'code_status' => 500,
                    'message' => 'Opération non aboutie !',
                    'status' => 'error',
                    'message_error' => $e->getMessage()
                ]);
            }
    }


}
