<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCollaborateurRequest;
use App\Quartier;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class CollaborateurController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function manage_clients() {
        $membres = User::whereHas('roles', function($q){
            $q->where('name', 'client');
        })->get();
        return view('backend.collaborateur.index')->with(compact('membres'));
    }

    public function load_member_for_payment(Request $request){
        $id = $request->input('id_user');
        $datasMember = User::find($id);
        $dataCentreInterets = User::find($request->input('id_user'))->centres_interets;
        $arrayResult = array_merge($datasMember->toArray(), [ "centres" => $dataCentreInterets]);
        header("Content-type: text/x-json");
        echo json_encode($arrayResult);
        exit();
    }

    /**
     * @param CreateCollaborateurRequest $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function createCollaborateur(CreateCollaborateurRequest $data){
        DB::beginTransaction();
        try {
            $num_abonne = "COLAB_".date('ymdmis');
            $user = User::create([
                'civilite' => $data['civilite'],
                'nom' => $data['nom'],
                'prenom' => $data['prenom'],
                'email' => $data['email'],
                'telephone' => trim($data['telephone']),
                'telephone2' => null,
                'datedenaisssance' => null,
                'commune_id' => null,
                'quartier_id' => null,
                'adresse' => null,
                'password' => Hash::make($data['password']),
                'num_abonne' => $num_abonne,
                'agence_id' => $data['agence_id'],
                'status' => 1
            ]);
            if($user) {
                $role_collaborateur = \App\Role::findOrFail(2);
                $user->attachRole($role_collaborateur);
                DB::commit();
                return response()->json([
                    'code_status' => 200,
                    'message' => 'Opération effectuée avec succès',
                    'status' => 'success',
                    'num_abonne' => $num_abonne
                ]);
            }
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'code_status' => 500,
                'message' => 'Opération non aboutie !',
                'status' => 'error',
                'message_error' => $e->getMessage()
            ]);
        }
    }


    public function updateCollaborateur(Request $request){

        $user = User::findOrFail($request->idUser);
        if ($request->ajax()) {
            DB::beginTransaction();
            try {

                $check =$user->update(['status' => $request->editStatus,'agence_id'=> $request->editAgence_id]);

                if ($check) {
                    DB::commit();

                    return response()->json([
                        'code_status' => 200,
                        'message' => 'Collaborateur modifié avec succès',
                        'status' => 'success',
                    ]);
                }

            } catch (\Exception $e) {

                DB::rollback();
                return response()->json([
                    'code_status' => 500,
                    'message' => 'Opération non aboutie !',
                    'status' => 'error',
                    'message_error' => $e->getMessage()
                ]);
            }

        }
    }

}
