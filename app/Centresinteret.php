<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Centresinteret extends Model {

    protected $fillable = [
        'centre_id',
        'centre_value',
    ];

    public function users() {
        return $this->belongsToMany(User::class);
    }

}
